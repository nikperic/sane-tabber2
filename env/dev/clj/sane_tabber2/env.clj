(ns sane-tabber2.env
  (:require
    [selmer.parser :as parser]
    [clojure.tools.logging :as log]
    [sane-tabber2.dev-middleware :refer [wrap-dev]]))

(def defaults
  {:init
   (fn []
     (parser/cache-off!)
     (log/info "\n-=[sane-tabber2 started successfully using the development profile]=-"))
   :stop
   (fn []
     (log/info "\n-=[sane-tabber2 has shut down successfully]=-"))
   :middleware wrap-dev})
