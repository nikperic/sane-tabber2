(ns sane-tabber2.env
  (:require [clojure.tools.logging :as log]))

(def defaults
  {:init
   (fn []
     (log/info "\n-=[sane-tabber2 started successfully]=-"))
   :stop
   (fn []
     (log/info "\n-=[sane-tabber2 has shut down successfully]=-"))
   :middleware identity})
