(ns sane-tabber2.statistics
  (:require
    [sane-tabber2.common :as common]))

(def safe+ (fnil + 0))

(defn ballot-filter [ks v]
  (filter #(contains? (-> %
                          (get-in (concat [:ballot] ks))
                          keys
                          set)
                      v)))

(defn ks-map [& ks]
  (map #(get-in % ks)))

(defn get-by-id [coll id k]
  (common/filter-first #(= (get % k) id) coll))

(defn team-ballots-filter [team-id]
  (ballot-filter [] (keyword team-id)))

(defn teams-ballots-filter [teams-id]
  (filter (fn [rd]
            (some (set (keys (:teams rd))) teams-id))))


(defn incremental-transducer [coll & transducers]
  (transduce (apply comp transducers) + coll))

(defn team-points-map
  [team-id]
  (map (fn [round-room]
         (.indexOf (->> (common/team-scores (:ballot round-room))
                        (sort-by val)
                        (map first))
                   (keyword team-id)))))

(defn team-points [round-data team-id]
  (incremental-transducer round-data (team-ballots-filter team-id) (team-points-map team-id)))

(defn team-scores-map
  [team-id]
  (mapcat #(->> (get-in % [:ballot team-id])
                (vals)
                (filter identity))))

(defn team-speaks [round-data team-id]
  (incremental-transducer round-data (team-ballots-filter team-id) (team-scores-map team-id)))

(defn speaker-scores-map
  [team-id speaker-n]
  (map #(get-in % [:ballot (keyword team-id) speaker-n])))

(defn speaker-score [team-id round-data speaker-n]
  (incremental-transducer round-data (team-ballots-filter team-id) (speaker-scores-map team-id speaker-n)))

(defn judge-seen-teams [round-data judge-id teams-id]
  (count (filter #(and (some (->> %
                                  :teams
                                  (keys)
                                  (map name)
                                  (set))
                             teams-id)
                       (contains? (set (:judges %)) judge-id))
                 round-data)))

(defn team-position-counts [round-data team-id]
  (frequencies
    (transduce (comp (team-ballots-filter team-id)
                     (map #(get-in % [:teams (keyword team-id)]))) conj round-data)))

(defn pairing-stats [round-data teams]
  (map (fn [{:keys [id]}]
         {:id            id
          :points        (team-points round-data id)
          :position-data (team-position-counts round-data id)})
       teams))