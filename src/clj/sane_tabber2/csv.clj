(ns sane-tabber2.csv
  (:require
    [clojure.data.csv :as csv]
    [clojure.java.io :as io]
    [ring.util.response :as response])
  (:import [java.io StringWriter]))

(defn csv-data->maps [csv-data]
  (map zipmap
       (->> (first csv-data)
            (map keyword)
            repeat)
       (rest csv-data)))

(defn csv-tempfile->maps [file]
  (-> file
      :tempfile
      (io/reader)
      (csv/read-csv)
      (csv-data->maps)))

(defn write-csv-to-str
  [map-sequence header]
  (let [writer (StringWriter.)
        data (map (fn [line]
                    (vec (map (fn [item]
                                (str (get line item)))
                              header)))
                  map-sequence)]
    (csv/write-csv writer (cons (map name header) data))
    (str writer)))

(defn as-csv [data csv-name]
  (let [resp (response/response data)
        disp (str "attachment; filename=\"" csv-name "\"")]
    (-> resp
        (response/header "content-disposition" disp)
        (response/content-type "text/csv;charset=utf-8"))))