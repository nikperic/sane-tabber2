(ns sane-tabber2.farady-pain)

;; This was fucking painful to discover.
;; Turns out faraday automatically converts all numbers to bigint/bigdecimals when reading numerals
;; EVEN IF you give it a base integer to start with.
;; Becomes a clusterfuck when you can't easily override it
;; So welcome to the wonderful work of with-redefs.
(defn ddb-num-str->num [^String s]
  (if (.contains s ".")
    (Double/parseDouble s)
    (Long/parseLong s)))