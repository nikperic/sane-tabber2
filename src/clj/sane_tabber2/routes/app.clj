(ns sane-tabber2.routes.app
  (:require
    [clojure.java.io :as io]
    [sane-tabber2.controllers.judges :as judges]
    [sane-tabber2.controllers.reports :as reports]
    [sane-tabber2.controllers.rooms :as rooms]
    [sane-tabber2.controllers.round-rooms :as round-rooms]
    [sane-tabber2.controllers.rounds :as rounds]
    [sane-tabber2.controllers.schools :as schools]
    [sane-tabber2.controllers.teams :as teams]
    [sane-tabber2.controllers.tournaments :as tournaments]
    [sane-tabber2.controllers.users :as users]
    [sane-tabber2.layout :as layout]
    [sane-tabber2.middleware :as middleware]
    [sane-tabber2.ws :as ws]
    [ring.util.response]
    [ring.util.http-response :as response]
    [reitit.ring.middleware.multipart :as multipart]))

(defn app-page [request]
  (layout/render request "app.html"))

(defn app-routes []
  [""
   {:middleware [middleware/wrap-csrf
                 middleware/wrap-formats]}
   ["/" {:get app-page}]
   ["/ws" {:get  (:ajax-get-or-ws-handshake-fn ws/connection)
           :post (:ajax-post-fn ws/connection)}]

   ["/public"
    ["/tournament/:tid"
     ["/registration-view" {:get app-page}]                 ;; public reg view
     ["/ballot/:round-room-id" {:get app-page}]             ;; open ballot input
     ]

    ["/api"
     ["/tournament/:tid"
      ["/registration-data" {:get tournaments/get-registration-data}]
      ["/ballot-data/:round-room-id" {:get rounds/get-ballot-data}]]
     ["/login" {:post users/login}]
     ["/logout" {:get users/logout}]]]

   ["/auth"
    ["" {:get app-page}]                                    ;; list tournaments
    ["/profile" {:get app-page}]                            ;; update user pass
    ["/tournaments"
     ["/create" {:get app-page}]]                           ;; create tournament
    ["/tournament/:tid"
     ["" {:get app-page}]                                   ;; list rounds
     ["/registration" {:get app-page}]                      ;; registration view
     ["/reporting" {:get app-page}]                         ;; reporting links
     ["/pairings/:round-id" {:get app-page}]                ;; pairings editor
     ["/ballots/:round-id" {:get app-page}]                 ;; ballots editor
     ]

    ["/api"
     ["/profile/update-password" {:post users/update-password}]
     ["/tournaments"
      ["" {:get        tournaments/get-user-tournaments
           :post       tournaments/create-tournament
           :middleware [multipart/multipart-middleware]}]
      ["/:tid"
       ["/judges"
        ["" {:get judges/get-tournament-judges}]
        ["/emails" {:delete judges/clear-judge-emails!}]]
       ["/rooms" {:get rooms/get-tournament-rooms}]
       ["/schools" {:get schools/get-tournament-schools}]
       ["/teams"
        ["" {:get teams/get-tournament-teams}]
        ["/stats" {:get teams/get-tournament-team-stats}]]
       ["/rounds"
        ["" {:get rounds/get-tournament-rounds}]
        ["/:round-id/rooms"
         ["" {:get round-rooms/get-round-rooms}]
         ["/mail-judges" {:get rounds/send-judge-emails}]
         ["/:round-room-id"
          ["/mail-judges" {:get rounds/send-round-room-judge-emails}]]]]
       ["/reports"
        ["/team-tab" {:get reports/team-tab-report}]
        ["/speaker-tab" {:get reports/speaker-tab-report}]
        ["/team-stats" {:get reports/team-stats-report}]
        ["/teams" {:get reports/teams-report}]
        ["/judges" {:get reports/judge-report}]
        ["/rooms" {:get reports/rooms-report}]
        ["/schools" {:get reports/schools-report}]
        ["/rounds/:round-id" {:get reports/round-pairings-report}]]]
      ]]]])
