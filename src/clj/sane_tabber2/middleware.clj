(ns sane-tabber2.middleware
  (:require
    [buddy.auth :as buddy]
    [buddy.auth.accessrules :as buddy.accessrules]
    [buddy.auth.backends.session :as buddy.session]
    [buddy.auth.middleware :as buddy.middleware]
    [sane-tabber2.env :refer [defaults]]
    [clojure.tools.logging :as log]
    [sane-tabber2.layout :refer [error-page]]
    [ring.middleware.anti-forgery :refer [wrap-anti-forgery]]
    [sane-tabber2.middleware.formats :as formats]
    [muuntaja.middleware :refer [wrap-format wrap-params]]
    [sane-tabber2.config :refer [env]]
    [ring.middleware.flash :refer [wrap-flash]]
    [ring.adapter.undertow.middleware.session :refer [wrap-session]]
    [ring.middleware.defaults :refer [site-defaults wrap-defaults]])
  )

(defn wrap-internal-error [handler]
  (fn [req]
    (try
      (handler req)
      (catch Throwable t
        (log/error t (.getMessage t))
        (error-page {:status  500
                     :title   "Something very bad has happened!"
                     :message "We've dispatched a team of highly trained gnomes to take care of the problem."})))))

(defn wrap-csrf [handler]
  (wrap-anti-forgery
    handler
    {:error-response
     (error-page
       {:status 403
        :title  "Invalid anti-forgery token"})}))

(defn restricted-error [request response]
  {:status  403
   :headers {"Content-Type" "text/plain"}
   :body    (str "Access to " (:uri request) " is not authorized")})

(defn wrap-restricted [handler]
  (buddy.accessrules/wrap-access-rules handler
                                       {:on-error restricted-error
                                        :rules    [{:uri     "/auth"
                                                    :handler buddy/authenticated?}
                                                   {:uri     "/auth/*"
                                                    :handler buddy/authenticated?}]}))


(defn wrap-formats [handler]
  (let [wrapped (-> handler wrap-params (wrap-format formats/instance))]
    (fn [request]
      ;; disable wrap-formats for websockets
      ;; since they're not compatible with this middleware
      ((if (:websocket? request) handler wrapped) request))))

(defn wrap-base [handler]
  (-> ((:middleware defaults) handler)
      (wrap-restricted)
      (buddy.middleware/wrap-authentication (buddy.session/session-backend))
      (wrap-flash)
      (wrap-session {:timeout      (* 8 60 60)              ;; 8 hours
                     :cookie-attrs {:http-only true}})
      (wrap-defaults
        (-> site-defaults
            (assoc-in [:security :anti-forgery] false)
            (dissoc :session)))
      (wrap-internal-error)))
