(ns sane-tabber2.db.core
  (:require
    [buddy.hashers :as hashers]
    [clojure.tools.logging :as log]
    [mount.core :as mount]
    [sane-tabber2.config :refer [env]]
    [taoensso.faraday :as far]
    [sane-tabber2.farady-pain :as pain])
  (:import
    [java.time LocalDateTime]
    [java.util UUID]))

(mount/defstate client-opts
  :start (:dynamo-client-opts env))

(def ^:private default-opts
  {:throughput {:read 4 :write 2}
   :block?     true})

;; ----------------------
;; Migrations
;; ----------------------

(def ^:const migration-table :_migrations)

(defn- ensure-migration-schema!
  [migration-table]
  (far/ensure-table client-opts migration-table
                    [:id :s]
                    default-opts))

(def ^:private migrations
  {"202012201530" {:create-tables [{:name    :judges
                                    :id-type :s
                                    :opts    default-opts}
                                   {:name    :resets
                                    :id-type :s
                                    :opts    default-opts}
                                   {:name    :rooms
                                    :id-type :s
                                    :opts    default-opts}
                                   {:name    :round-rooms
                                    :id-type :s
                                    :opts    default-opts}
                                   {:name    :rounds
                                    :id-type :s
                                    :opts    default-opts}
                                   {:name    :schools
                                    :id-type :s
                                    :opts    default-opts}
                                   {:name    :teams
                                    :id-type :s
                                    :opts    default-opts}
                                   {:name    :tournaments
                                    :id-type :s
                                    :opts    default-opts}
                                   {:name    :users
                                    :id-type :s
                                    :opts    default-opts}]
                   :seed          [{:table :users
                                    :data  [{:id       "admin"
                                             :email    "nik@nikperic.com"
                                             :password (hashers/derive "password!")}]}]}})

(defn- migrate!
  [id {:keys [create-tables seed]}]
  (doseq [{:keys [name id-type opts]} create-tables]
    (far/ensure-table client-opts name [:id id-type] opts))
  (doseq [{:keys [table data]} seed
          entry data]
    (far/put-item client-opts table entry))
  (far/put-item client-opts migration-table {:id         id
                                             :created-on (far/freeze (LocalDateTime/now))}))

(defn run-migrations!
  []
  (log/info "Starting migrations")
  (ensure-migration-schema! migration-table)
  (let [applied-ids (->> (far/scan client-opts migration-table)
                         (map :id)
                         (set))]
    (doseq [[id migration] migrations]
      (when-not (contains? applied-ids id)
        (log/info "Running migration" id)
        (migrate! id migration)
        (log/info "Finished migration" id))))
  (log/info "Finished migrations"))

;; ----------------------
;; API
;; ----------------------

(defn uuid-str
  []
  (str (UUID/randomUUID)))

(defn rand-uuid-str-when-nil
  [x]
  (or x (uuid-str)))

(defn scan
  [table & [opts]]
  (with-redefs [taoensso.faraday/ddb-num-str->num pain/ddb-num-str->num]
    (far/scan client-opts table opts)))

(defn get-item
  [table conds & [opts]]
  (with-redefs [taoensso.faraday/ddb-num-str->num pain/ddb-num-str->num]
    (far/get-item client-opts table conds opts)))

(defn batch-get-item
  [requests & [opts]]
  (far/batch-get-item client-opts requests opts))

(defn put-item
  [table item & [opts]]
  (far/put-item client-opts table item opts))

(defn update-item
  [table conds & [opts]]
  (far/update-item client-opts table conds opts))

(defn delete-item
  [table item & [opts]]
  (far/delete-item client-opts table item opts))

(defn transact-write-items
  [request]
  (far/transact-write-items client-opts request))



