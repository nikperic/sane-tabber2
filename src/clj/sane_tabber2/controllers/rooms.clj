(ns sane-tabber2.controllers.rooms
  (:require
    [ring.util.http-response :as http-response]
    [sane-tabber2.db.core :as db]
    [sane-tabber2.ws :as ws]))

;; DB

(defn tournament-rooms
  [tournament-id]
  (db/scan :rooms {:attr-conds {:tournament-id [:eq tournament-id]}}))

(defn put-room
  [room]
  (let [room (update room :id db/rand-uuid-str-when-nil)]
    (db/put-item :rooms room)
    room))

(defn batch-create-rooms
  [rooms]
  (doseq [room rooms]
    (put-room (update room :id db/rand-uuid-str-when-nil))))

;; Endpoints

(defn get-tournament-rooms
  [req]
  (-> req
      (get-in [:path-params :tid])
      (tournament-rooms)
      (http-response/ok)))

(defmethod ws/handle-message!
  :tournament/create-room
  [{:keys [?data]}]
  (ws/send-msg (ws/tournament-sub-id ?data)
               [:tournament/add-room (put-room (dissoc ?data :tid))]))

(defmethod ws/handle-message!
  :tournament/update-room
  [{:keys [?data]}]
  (ws/send-msg (ws/tournament-sub-id ?data)
               [:tournament/update-room (put-room (dissoc ?data :tid))]))