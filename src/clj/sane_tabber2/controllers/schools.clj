(ns sane-tabber2.controllers.schools
  (:require
    [ring.util.http-response :as http-response]
    [sane-tabber2.db.core :as db]
    [sane-tabber2.ws :as ws]))

;; DB

(defn tournament-schools
  [tournament-id]
  (db/scan :schools {:attr-conds {:tournament-id [:eq tournament-id]}}))

(defn put-school
  [school]
  (let [school (update school :id db/rand-uuid-str-when-nil)]
    (db/put-item :schools school)
    school))

(defn batch-create-schools
  [schools]
  (doseq [school schools]
    (put-school (update school :id db/rand-uuid-str-when-nil))))

;; Endpoints

(defn get-tournament-schools
  [req]
  (-> req
      (get-in [:path-params :tid])
      (tournament-schools)
      (http-response/ok)))

(defmethod ws/handle-message!
  :tournament/create-school
  [{:keys [?data]}]
  (ws/send-msg (ws/tournament-sub-id ?data)
               [:tournament/add-school (put-school (dissoc ?data :tid))]))

(defmethod ws/handle-message!
  :tournament/update-school
  [{:keys [?data]}]
  (ws/send-msg (ws/tournament-sub-id ?data)
               [:tournament/update-school (put-school (dissoc ?data :tid))]))