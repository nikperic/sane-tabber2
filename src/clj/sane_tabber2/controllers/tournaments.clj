(ns sane-tabber2.controllers.tournaments
  (:require
    [sane-tabber2.controllers.judges :as judges]
    [sane-tabber2.controllers.rooms :as rooms]
    [sane-tabber2.controllers.schools :as schools]
    [sane-tabber2.controllers.teams :as teams]
    [sane-tabber2.csv :as csv]
    [sane-tabber2.db.core :as db]
    [sane-tabber2.utils :as utils]
    [ring.util.http-response :as response]))

;; DB

(defn user-tournaments
  [user-id]
  (db/scan :tournaments {:attr-conds {:users [:contains user-id]}}))

(defn put-tournament
  [tournament]
  (let [tournament (update tournament :id db/rand-uuid-str-when-nil)]
    (db/put-item :tournaments tournament)
    tournament))

(defn get-tournament
  [tournament-id]
  (db/get-item :tournaments {:id tournament-id}))

;; Endpoints

(defn get-user-tournaments
  [req]
  (response/ok (user-tournaments (utils/req-user-id req))))

;; TODO: csv validation!! Key if we're to make this public
(defn create-tournament
  [req]
  (let [user-id       (utils/req-user-id req)
        {:keys [title team-count speaker-count rooms-file schools-file judges-file teams-file]} (:params req)
        tournament-id (:id (put-tournament {:title         title
                                            :owner         user-id
                                            :users         [user-id]
                                            :team-count    (Integer/parseInt team-count)
                                            :speaker-count (Integer/parseInt speaker-count)}))]
    (judges/batch-create-judges (map #(-> %
                                          (assoc :tournament-id tournament-id
                                                 :signed-in? false
                                                 :accessible? false)
                                          (update :rating (fn [r] (Integer/parseInt r))))
                                     (csv/csv-tempfile->maps judges-file)))
    (rooms/batch-create-rooms (map #(assoc % :tournament-id tournament-id
                                             :in-use? true
                                             :accessible? false)
                                   (csv/csv-tempfile->maps rooms-file)))
    (schools/batch-create-schools (map #(assoc % :tournament-id tournament-id)
                                       (csv/csv-tempfile->maps schools-file)))
    (let [schools-by-code (group-by :code (schools/tournament-schools tournament-id))]
      (teams/batch-create-teams (map #(-> %
                                          (assoc :tournament-id tournament-id
                                                 :school-id (get-in schools-by-code [(:school-code %) 0 :id])
                                                 :signed-in? false
                                                 :accessible? false)
                                          (dissoc :school-code))
                                     (csv/csv-tempfile->maps teams-file))))
    (response/found "/auth")))

(defn get-registration-data
  [req]
  (if-some [tid (get-in req [:path-params :tid])]
    (response/ok {:schools (schools/tournament-schools tid)
                  :teams   (teams/tournament-teams tid)})
    (response/not-found)))