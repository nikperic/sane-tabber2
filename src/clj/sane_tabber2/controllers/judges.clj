(ns sane-tabber2.controllers.judges
  (:require
    [ring.util.http-response :as http-response]
    [sane-tabber2.db.core :as db]
    [sane-tabber2.ws :as ws]))

;; DB

(defn tournament-judges
  [tournament-id]
  (db/scan :judges {:attr-conds {:tournament-id [:eq tournament-id]}}))

(defn put-judge
  [judge]
  (let [judge (update judge :id db/rand-uuid-str-when-nil)]
    (db/put-item :judges judge)
    judge))

(defn batch-create-judges
  [judges]
  (doseq [judge judges]
    (put-judge (update judge :id db/rand-uuid-str-when-nil))))

(defn batch-remove-judge-emails!
  [tournament-id]
  (doseq [judge (tournament-judges tournament-id)]
    (put-judge (assoc judge :email nil)))
  "May you never worry about data breaches")

;; Endpoints

(defn get-tournament-judges
  [req]
  (-> req
      (get-in [:path-params :tid])
      (tournament-judges)
      (http-response/ok)))

(defn clear-judge-emails!
  [req]
  (-> req
      (get-in [:path-params :tid])
      (batch-remove-judge-emails!)
      (http-response/ok)))

(defmethod ws/handle-message!
  :tournament/create-judge
  [{:keys [?data]}]
  (ws/send-msg (ws/tournament-sub-id ?data)
               [:tournament/add-judge (put-judge (dissoc ?data :tid))]))

(defmethod ws/handle-message!
  :tournament/update-judge
  [{:keys [?data]}]
  (ws/send-msg (ws/tournament-sub-id ?data)
               [:tournament/update-judge (put-judge (dissoc ?data :tid))]))