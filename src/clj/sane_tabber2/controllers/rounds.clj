(ns sane-tabber2.controllers.rounds
  (:require
    [clojure.tools.logging :as log]
    [ring.util.http-response :as http-response]
    [sane-tabber2.controllers.judges :as judges]
    [sane-tabber2.controllers.rooms :as rooms]
    [sane-tabber2.controllers.round-rooms :as round-rooms]
    [sane-tabber2.controllers.schools :as schools]
    [sane-tabber2.controllers.teams :as teams]
    [sane-tabber2.db.core :as db]
    [sane-tabber2.pairings :as pairings]
    [sane-tabber2.mail :as mail]
    [sane-tabber2.ws :as ws]
    [ring.util.http-response :as response]
    [sane-tabber2.common :as common]
    [clojure.string :as string]))

;; DB

(defn tournament-rounds
  [tournament-id]
  (db/scan :rounds {:attr-conds {:tournament-id [:eq tournament-id]}}))

(defn get-round
  [round-id]
  (db/get-item :rounds {:id round-id}))

(defn put-round
  [round]
  (let [round (update round :id db/rand-uuid-str-when-nil)]
    (db/put-item :rounds round)
    round))

(defn batch-create-rounds
  [rounds]
  (doseq [round rounds]
    (put-round (update round :id db/rand-uuid-str-when-nil))))

(defn delete-round
  [round-id]
  (round-rooms/delete-round-rooms round-id)
  (db/delete-item :rounds {:id round-id}))

;; Impl

(defn autopair-round [{:keys [tid round-id]}]
  (log/info "Autopairing round for tournament" tid "and round" round-id)
  (round-rooms/delete-round-rooms round-id)
  (let [teams       (filter :signed-in? (teams/tournament-teams tid))
        judges      (filter :signed-in? (judges/tournament-judges tid))
        rooms       (filter :in-use? (rooms/tournament-rooms tid))
        round-rooms (round-rooms/all-scored-round-rooms tid)
        round       (get-round round-id)]
    (doseq [pairing (pairings/pair-round teams judges rooms round-rooms)]
      (round-rooms/put-round-room (assoc pairing :tournament-id tid
                                                 :round-id round-id)))
    (put-round (assoc round :status "paired"))))

;; Endpoints

(defn get-tournament-rounds
  [req]
  (-> req
      (get-in [:path-params :tid])
      (tournament-rounds)
      (http-response/ok)))

(defn get-ballot-data
  [req]
  (let [{:keys [tid round-room-id]} (:path-params req)]
    (let [rr (round-rooms/round-room round-room-id)]
      (if (and (some? rr)
               (nil? (:ballot rr)))
        (response/ok
          {:tournament/rooms                (rooms/tournament-rooms tid)
           :tournament/judges               (map #(select-keys % [:id :name]) (judges/tournament-judges tid))
           :tournament/teams                (teams/tournament-teams tid)
           :tournament/schools              (schools/tournament-schools tid)
           :public.ballot/active-round-room rr})
        (response/not-found "Ballot does not exist or has already been filled out")))))

(defn ballot-emailer
  [{:keys [tid round-rooms judges tournament-name round-n]}]
  (doseq [{rr-judges     :judges
           round-room-id :id} round-rooms
          judge-id rr-judges
          :let [judge-email (:email (common/get-first-by-id judges judge-id))]]
    (when-not (string/blank? judge-email)
      (mail/send-email! judge-email
                        (mail/judge-subject tournament-name round-n)
                        (mail/judge-mail-content tid round-room-id)))))

(defn send-judge-emails
  [req]
  ;; TODO: can combine this and send-round-room-judge-emails, only round-rooms differs.
  (let [{:keys [tid round-id]} (:path-params req)
        judges          (judges/tournament-judges tid)
        tournament-name (:title (db/get-item :tournaments {:id tid}))
        round-rooms     (round-rooms/round-rooms round-id)
        round-n         (:round-number (get-round round-id))]
    (ballot-emailer {:tid             tid
                     :round-rooms     round-rooms
                     :judges          judges
                     :tournament-name tournament-name
                     :round-n         round-n})
    (response/content-type (response/ok "sent")
                           "text/html; charset=utf-8")))

(defn send-round-room-judge-emails
  [req]
  (let [{:keys [tid round-id round-room-id]} (:path-params req)
        judges          (judges/tournament-judges tid)
        tournament-name (:title (db/get-item :tournaments {:id tid}))
        round-rooms     [(round-rooms/round-room round-room-id)]
        round-n         (:round-number (get-round round-id))]
    (ballot-emailer {:tid             tid
                     :round-rooms     round-rooms
                     :judges          judges
                     :tournament-name tournament-name
                     :round-n         round-n})
    (response/content-type (response/ok "sent")
                           "text/html; charset=utf-8")))

(defmethod ws/handle-message!
  :tournament/create-round
  [{:keys [?data]}]
  (let [tid (:tid ?data)]
    (ws/send-msg (ws/tournament-sub-id ?data)
                 [:tournament/add-round (put-round {:tournament-id tid
                                                    :round-number  (inc (count (tournament-rounds tid)))
                                                    :status        :unpaired})])))

(defmethod ws/handle-message!
  :tournament/delete-round
  [{:keys [?data]}]
  (let [round-id (:round-id ?data)]
    (delete-round round-id)
    (ws/send-msg (ws/tournament-sub-id ?data)
                 [:tournament/remove-round round-id])))

(defmethod ws/handle-message!
  :tournament/auto-pair-round
  [{:keys [?data]}]
  (ws/send-msg (ws/tournament-sub-id ?data)
               [:tournament/finished-auto-pairing (autopair-round ?data)]))
