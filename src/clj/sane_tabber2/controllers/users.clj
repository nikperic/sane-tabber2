(ns sane-tabber2.controllers.users
  (:require
    [buddy.hashers :as hashers]
    [ring.util.http-response :as http]
    [ring.util.response :as response]
    [sane-tabber2.db.core :as db]
    [sane-tabber2.utils :as utils])
  (:import org.apache.commons.codec.binary.Base64
           java.nio.charset.Charset))

;; DB

(defn update-password!
  [user-id password]
  (db/update-item :users
                  {:id user-id}
                  {:update-expr    "SET password = :password"
                   :expr-attr-vals {":password" (hashers/encrypt password)}}))

(defn get-user
  [user-id]
  (db/get-item :users {:id user-id}))

;; AUTHENTICATION

(def redirect-on-auth "/auth")

(defn authenticate [id]
  (update (response/redirect redirect-on-auth) :session assoc :identity id))

(defn auth [req]
  (get-in req [:headers "authorization"]))

(defn decode-auth [encoded]
  (let [auth (second (.split encoded " "))]
    (-> (Base64/decodeBase64 auth)
        (String. (Charset/forName "UTF-8"))
        (.split ":"))))

(defmacro timed [exp timeout]
  `(let [start-time# (System/currentTimeMillis)
         result#     (try ~exp (catch Exception e# e#))
         end-time#   (System/currentTimeMillis)
         wait-time#  (- ~timeout (- end-time# start-time#))]
     (when (pos? wait-time#)
       (Thread/sleep wait-time#))
     (if (instance? Exception result#)
       (throw result#)
       result#)))

;; make sure that login is handled in constant time
;; prevents https://en.wikipedia.org/wiki/Timing_attack
(defn authenticated-user-id [[user-id pass]]
  (timed
    (when-let [user (get-user user-id)]
      (when (hashers/check pass (:password user))
        (:id user)))
    300))

;; ENDPOINTS

(defn login
  [req]
  (if-let [user-id (authenticated-user-id (decode-auth (auth req)))]
    (-> req
        (get-in [:params :redirect])
        (response/response)
        (update :session assoc :identity user-id))
    (http/unauthorized {:error "invalid login"})))

(defn logout
  [_]
  (-> (response/redirect "/")
      (update :session dissoc :identity)))

(defn update-password
  [req]
  (let [user-id (utils/req-user-id req)]
    (when-let [user (get-user user-id)]
      (let [{:keys [new-password current-password]} (:params req)]
        (if (hashers/check current-password (:password user))
          (do (update-password! user-id new-password)
              (http/ok))
          (http/forbidden))))))