(ns sane-tabber2.controllers.teams
  (:require
    [ring.util.http-response :as http-response]
    [sane-tabber2.controllers.round-rooms :as round-rooms]
    [sane-tabber2.db.core :as db]
    [sane-tabber2.ws :as ws]
    [sane-tabber2.statistics :as stats]))

;; DB

(defn tournament-teams
  [tournament-id]
  (db/scan :teams {:attr-conds {:tournament-id [:eq tournament-id]}}))

(defn put-team
  [team]
  (let [team (update team :id db/rand-uuid-str-when-nil)]
    (db/put-item :teams team)
    team))

(defn batch-create-teams
  [teams]
  (doseq [team teams]
    (put-team (update team :id db/rand-uuid-str-when-nil))))

;; Endpoints

(defn get-tournament-teams
  [req]
  (-> req
      (get-in [:path-params :tid])
      (tournament-teams)
      (http-response/ok)))

(defn get-tournament-team-stats
  [req]
  (let [tid (get-in req [:path-params :tid])
        teams (tournament-teams tid)
        scored-rrs (round-rooms/all-scored-round-rooms tid)]
    (http-response/ok (stats/pairing-stats scored-rrs teams))))

(defmethod ws/handle-message!
  :tournament/create-team
  [{:keys [?data]}]
  (ws/send-msg (ws/tournament-sub-id ?data)
               [:tournament/add-team (put-team (dissoc ?data :tid))]))

(defmethod ws/handle-message!
  :tournament/update-team
  [{:keys [?data]}]
  (ws/send-msg (ws/tournament-sub-id ?data)
               [:tournament/update-team (put-team (dissoc ?data :tid))]))