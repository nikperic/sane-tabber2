(ns sane-tabber2.controllers.round-rooms
  (:require
    [ring.util.http-response :as http-response]
    [sane-tabber2.db.core :as db]
    [sane-tabber2.mail :as mail]
    [sane-tabber2.ws :as ws]
    [ring.util.http-response :as response]))

;; DB

(defn tournament-round-rooms
  [tournament-id]
  (db/scan :round-rooms {:attr-conds {:tournament-id [:eq tournament-id]}}))

(defn round-rooms
  [round-id]
  (db/scan :round-rooms {:attr-conds {:round-id [:eq round-id]}}))

(defn round-room
  [round-room-id]
  (db/get-item :round-rooms {:id round-room-id}))

(defn scored-round-rooms
  [round-id]
  (db/scan :round-rooms {:attr-conds {:round-id [:eq round-id]
                                      :ballot   [:ne nil]}}))

(defn all-scored-round-rooms
  [tournament-id]
  (db/scan :round-rooms {:attr-conds {:tournament-id [:eq tournament-id]
                                      :ballot        [:ne nil]}}))

(defn put-round-room
  [round-room]
  (let [round-room (update round-room :id db/rand-uuid-str-when-nil)]
    (db/put-item :round-rooms round-room)
    round-room))

(defn delete-round-rooms
  [round-id]
  (let [to-delete (map (fn [round-room]
                         {:id (:id round-room)})
                       (round-rooms round-id))]
    (when (not-empty to-delete)
      (doseq [rr to-delete]
        (db/delete-item :round-rooms rr)))))

;; Endpoints

(defn get-round-rooms
  [req]
  (-> req
      (get-in [:path-params :round-id])
      (round-rooms)
      (http-response/ok)))

(defmethod ws/handle-message!
  :round/create-round-room
  [{:keys [?data]}]
  (ws/send-msg (ws/tournament-sub-id ?data)
               [:round/add-round-room (put-round-room (dissoc ?data :tid))]))

(defmethod ws/handle-message!
  :round/update-round-room
  [{:keys [?data]}]
  (prn ?data)
  (ws/send-msg (ws/tournament-sub-id ?data)
               [:round/update-round-room (put-round-room (dissoc ?data :tid))]))