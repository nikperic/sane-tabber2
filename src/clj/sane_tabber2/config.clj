(ns sane-tabber2.config
  (:require
    [cprop.core :as cprop]
    [cprop.source :as source]
    [mount.core :as mount]))

(mount/defstate env
  :start
  (cprop/load-config
    :merge
    [(mount/args)
     (source/from-system-props)
     (source/from-env)]))
