(ns sane-tabber2.mail
  (:require
    [clojure.tools.logging :as log]
    [sane-tabber2.config :refer [env]]
    [sendgrid.core :as sendgrid]))

(defn judge-mail-content
  [tid round-room-id]
  (let [url (str (:email-domain env) "/public/tournament/" tid "/ballot/" round-room-id)]
    (str "<p>Hello,</p>"
         "<p>Please complete this round's ballot located at: </p>"
         "<p><strong><a href='" url "'>Round ballot</a></strong></p>"
         "<p>or paste in the URL: " url "</p>"
         "<p>While completing the ballot, follow the tournament's guidelines for scoring.</p>"
         "<p>Thank you for your support as a judge!</p>")))

(defn judge-subject
  [tournament-name round-n]
  (str tournament-name " - Round " round-n " ballot"))

(defn send-email!
  [to subject message]
  (try
    (sendgrid/send-email {:api-token (str "Bearer " (:sendgrid-api-token env))
                          :from      "no-reply@sanetabber.com"
                          :to        to
                          :subject   subject
                          :message   message})
    (catch Exception e
      (log/error "Unable to send email to" to ". Full exception" e))))