(ns sane-tabber2.ws
  (:require
    [clojure.string :as string]
    [clojure.tools.logging :as log]
    [mount.core :as mount]
    [taoensso.sente :as sente]
    [taoensso.sente.server-adapters.undertow :refer [get-sch-adapter]]
    [sane-tabber2.utils :as utils])
  (:import
    [java.util UUID]))

(def ^:const default-subs #{:global})
(declare connection)

;; Communication protocol
(mount/defstate subscriptions
  :start (atom {})
  :stop (reset! subscriptions {}))

(defn subscribed-uids [channel]
  (filter (fn [uid]
            (contains? (get @subscriptions uid) channel))
          (:any (deref (:connected-uids connection)))))

(defn client-id [ring-req]
  (let [user-id (or (utils/req-user-id ring-req) "guest")]
    (if (contains? (set (subscribed-uids :global)) user-id)
      (str user-id "-st2_" (UUID/randomUUID))
      user-id)))

(defn base-user-id [s]
  (first (string/split s "-st2_")))

(defn subscribed-users [channel]
  (map
    #(hash-map :uid %)
    (subscribed-uids channel)))

(defn tournament-sub-id [?data]
  (keyword "tournament" (str "_" (:tid ?data))))

(defn send-msg
  ([msg]
   (send-msg :global msg))
  ([channel msg]
   (send-msg channel msg identity))
  ([channel msg uid-filter-fn]
   (doseq [uid (filter uid-filter-fn (subscribed-uids channel))]
     ((:send-fn connection) uid msg))))

(mount/defstate connection
  :start (sente/make-channel-socket!
           (get-sch-adapter)
           {:packer     :edn
            :user-id-fn client-id}))

;; Message handler
(defmulti handle-message!
          (fn [{:keys [id uid ?data]}]
            (when (some? ?data)                             ;; Don't show nil messages, typically connectivity and pings
              (log/debug "Message received from" uid ":" ?data))
            id))

(defmethod handle-message! :comm/reset-subs
  [{new-channel :?data
    :keys       [uid]}]
  (log/debug uid "clearing subs and resetting for" new-channel)
  (swap! subscriptions assoc uid (conj default-subs new-channel)))

(defn ensure-connectivity! [{:keys [uid ?data]}]
  (when-let [ns (:response-channel ?data)]
    (when-not (contains? (get @subscriptions uid) ns)
      (swap! subscriptions update uid conj ns))))

;; chsk events
(defmethod handle-message! :chsk/bad-package
  [{:keys [?data]}]
  (log/warn "Bad package" ?data))

(defmethod handle-message! :chsk/bad-event
  [{:keys [?data]}]
  (log/warn "Bad event" (pr-str ?data)))

(defmethod handle-message! :chsk/uidport-open
  [{:keys [?data]}]
  (log/debug "WS connection open for uid" ?data)
  (swap! subscriptions assoc ?data default-subs))

(defn reconcile-connected-uids [subs]
  (let [connected-uids (->> connection :connected-uids deref :any)]
    (select-keys subs (seq connected-uids))))

(defmethod handle-message! :chsk/uidport-close
  [{:keys [?data]}]
  (log/debug "WS connection close for uid" ?data)
  (swap! subscriptions reconcile-connected-uids))

(defmethod handle-message! :chsk/ws-ping [_])

;; Error handler
(defn handle-error! [{:keys [id uid] :as req} ^Exception e]
  (log/error "WS Exception" e)
  (send-msg :global
            [:common/message
             {:type    :error
              :message (str "Error on command " id ". Cause:" (:cause (Throwable->map e)))}]
            #(= uid %)))

;; Catchall
(defmethod handle-message! :default
  [{:keys [id uid client-id ?data] :as msg}]
  (log/error (IllegalArgumentException.) "Unknown WS protocol" id "with data" ?data "from uid" uid))

(defn stop-router! [stop-fn]
  (when stop-fn (stop-fn)))

(defn start-router! []
  (sente/start-chsk-router! (:ch-recv connection)
                            (fn [msg]
                              (try
                                (do
                                  (ensure-connectivity! msg)
                                  (handle-message! (update msg :?data (fn [data]
                                                                        (if (map? data)
                                                                          (dissoc data :response-channel)
                                                                          data)) )))
                                (catch Exception e
                                  (handle-error! msg e))))))

(mount/defstate router
  :start (start-router!)
  :stop (router))
