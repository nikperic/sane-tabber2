(ns sane-tabber2.utils)

(defn req-user-id
  [req]
  (get-in req [:session :identity]))