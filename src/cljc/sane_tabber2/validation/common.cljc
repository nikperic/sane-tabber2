(ns sane-tabber2.validation.common
  (:require
    [clojure.string :as string]))

(defn basic-error-message
  [[k message] custom?]
  (str (when-not custom?
         (str (string/capitalize (name k))
              ": "))
       message))

(def exact-count
  (letfn [(validate [v exact]
            {:pre [(number? exact)]}
            (= (count v) exact))]
    {:message  "not equal to the required length %s"
     :optional true
     :validate validate}))

(def date
  (letfn [(validate [v]
            #?(:clj (inst? v)
               :cljs (instance? js/Date v)))]
    {:message  "not a valid date %s"
     :optional true
     :validate validate}))