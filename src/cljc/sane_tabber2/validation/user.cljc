(ns sane-tabber2.validation.user
  (:require
    [struct.core :as st]))

(def +user+
  [[:username st/required]
   [:password st/required]])

(defn validate-user [params]
  (first (st/validate params +user+)))