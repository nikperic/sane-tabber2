(ns sane-tabber2.common
  (:require
    [clojure.string :as string]))

(def filter-first (comp first filter))
(defn index-of
  ([xs x]
   #?(:clj  (.indexOf xs x)
      :cljs (index-of xs x 0)))
  ([xs x idx]
   #?(:clj  (.indexOf xs x idx)
      :cljs (cond
              (empty? xs) -1
              (= x (first xs)) idx
              :else (recur (rest xs) x (inc idx))))))

(defn get-by-id
  ([list id id-kw]
   (filter #(= id (id-kw %)) list))
  ([list id]
   (get-by-id list id :id)))

(def get-first-by-id (comp first get-by-id))

(defn blank-str->nil [s]
  (when-not (string/blank? s) s))

(defn blank-strs->nil [m kws]
  (reduce
    (fn [m k]
      (update m k (fn [v] (when-not (string/blank? v) v))))
    m
    kws))

(defn team-scores
  [scores]
  (reduce-kv (fn [out k v]
               (assoc out k (->> v (vals) (filter identity) (apply +))))
             {} scores))