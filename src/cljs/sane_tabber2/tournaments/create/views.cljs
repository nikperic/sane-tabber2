(ns sane-tabber2.tournaments.create.views
  (:require
    [clojure.string :as string]
    [reagent.core :as reagent]
    [sane-tabber2.utils :as utils]
    [syn-antd.button :as button]
    [syn-antd.col :as col]
    [syn-antd.form :as form]
    [syn-antd.input :as input]
    [syn-antd.row :as row]
    [syn-antd.space :as space]))

(defn create-tournament-page
  []
  (let [tournament-name (reagent/atom "")]
    (fn []
      [space/space
       {:orientation :vertical}
       [row/row
        {:gutter 32}
        [col/col
         {:span 14}
         [:form#new-tournament-form
          {:action   "/auth/api/tournaments"
           :enc-type "multipart/form-data"
           :method   "post"}
          [:input {:type "hidden" :name "__anti-forgery-token" :value js/csrfToken}]
          [form/form-item
           {:label "Tournament Name"}
           [input/input
            {:name      "title"
             :value     @tournament-name
             :on-change (fn [e]
                          (reset! tournament-name (utils/target-value e)))}]]
          [:input {:type "hidden" :name "team-count" :value 2}] ;; TODO
          [:input {:type "hidden" :name "speaker-count" :value 2}] ;; TODO
          ;; TODO: ensure files present
          [form/form-item
           {:label "Rooms CSV Files"}
           [:input {:type "file" :name "rooms-file"}]]
          [form/form-item
           {:label "Schools CSV Files"}
           [:input {:type "file" :name "schools-file"}]]
          [form/form-item
           {:label "Judges CSV Files"}
           [:input {:type "file" :name "judges-file"}]]
          [form/form-item
           {:label "Teams CSV Files"}
           [:input {:type "file" :name "teams-file"}]]
          [button/button
           {:type      "primary"
            :html-type "submit"
            :disabled  (string/blank? @tournament-name)}
           "Create Tournament"]]]
        [col/col
         {:span 10}
         [:div
          [:p "Please use the attached templates as starting points for populating data for your new tournament. It's much
    faster to just edit this data in a spreadsheet than any tool such as this one... so you may as well do that."]
          [:p "If you have more or less speakers per team, you are welcome to add/remove columns as necessary from
    the teams template."]
          [:p "Otherwise, please ensure you adhere to the format in the attached templates... should you fail, the computer
    may ridicule you."]]
         [button/button
          {:block true
           :href  "/csv/rooms-template.csv"}
          "Rooms"]
         [button/button
          {:block true
           :href  "/csv/schools-template.csv"}
          "Schools"]
         [button/button
          {:block true
           :href  "/csv/judges-template.csv"}
          "Judges"]
         [button/button
          {:block true
           :href  "/csv/teams-template.csv"}
          "Teams"]]]])))