(ns sane-tabber2.tournaments.views
  (:require
    [kee-frame.core :as kee-frame]
    [reagent.core :as reagent]
    [re-frame.core :as re-frame]
    [syn-antd.button :as button]
    [syn-antd.list :as list]
    [syn-antd.space :as space]))

(defn tournaments-page
  []
  (let [tournaments (re-frame/subscribe [:tournaments/user-tournaments])]
    (fn []
      [space/space
       {:direction "vertical"
        :style     {:width "100%"}}
       [button/button
        {:href (kee-frame/path-for [:tournaments.create])
         :type "primary"}
        "Create new tournament"]
       [list/list
        {:bordered    true
         :dataSource  (sort-by :title @tournaments)
         :render-item (fn [item]
                        (reagent/as-element
                          [:a.hoverable {:href (kee-frame/path-for [:tournament.rounds {:tid (aget item "id")}])}
                           [list/list-item (aget item "title")]]))}]])))