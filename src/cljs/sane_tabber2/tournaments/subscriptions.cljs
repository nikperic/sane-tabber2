(ns sane-tabber2.tournaments.subscriptions
  (:require
    [re-frame.core :as re-frame]
    [reframe-utils.core :as rf-utils]))

(rf-utils/multi-generation
  rf-utils/reg-basic-sub
  :tournaments/user-tournaments)