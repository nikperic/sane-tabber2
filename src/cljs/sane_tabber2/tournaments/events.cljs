(ns sane-tabber2.tournaments.events
  (:require
    [kee-frame.core :as kee-frame]
    [re-frame.core :as re-frame]
    [reframe-utils.core :as rf-utils]))

(rf-utils/multi-generation
  rf-utils/reg-ajax-get-event
  ["/auth/api/tournaments" :tournaments/user-tournaments])

(re-frame/reg-event-fx
  :tournaments/init-page
  (fn [_ _]
    {:dispatch [:tournaments/get-user-tournaments]}))

(kee-frame/reg-controller
  :tournaments/tournaments-controller
  {:params (fn [route-data]
             (when (-> route-data
                       (get-in [:data :name])
                       (= :tournaments))
               true))
   :start  [:tournaments/init-page]})