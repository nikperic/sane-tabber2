(ns sane-tabber2.utils
  (:require [reagent.dom :as dom])
  (:import [goog.async Throttle Debouncer]))

(defn target-value [e] (.. e -target -value))

(defn disposable->function [disposable listener interval]
  (let [disposable-instance (disposable. listener interval)]
    (fn [& args]
      (.apply (.-fire disposable-instance) disposable-instance (to-array args)))))

(defn throttle [listener interval]
  (disposable->function Throttle listener interval))

(defn debounce [listener interval]
  (disposable->function Debouncer listener interval))

(def with-focus
  (with-meta identity
             {:component-did-mount #(when-let [el (dom/dom-node %)]
                                      (.focus el))}))

(defn safe-name
  [x]
  (when (some? x) (name x)))