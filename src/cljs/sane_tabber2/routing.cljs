(ns sane-tabber2.routing
  (:require
    [re-frame.core :as re-frame]))

(def routes
  [["/" :auth]
   ["/public"
    ["/tournament/:tid"
     ["/registration-view" :public.tournament.registration-view]
     ["/ballot/:round-room-id" :public.tournament.ballot-input]]]
   ["/auth"
    ["" :tournaments]
    ["/profile" :profile]
    ["/tournaments/create" :tournaments.create]
    ["/tournament/:tid"
     ["" :tournament.rounds]
     ["/registration" :tournament.registration]
     ["/reporting" :tournament.reporting]
     ["/pairings/:round-id" :tournament.pairings]
     ["/ballots/:round-id" :tournament.ballots]]]])

(re-frame/reg-sub
  :nav/route
  :<- [:kee-frame/route]
  identity)

(re-frame/reg-event-fx
  :nav/route-name
  (fn [_ [_ route-name]]
    {:navigate-to [route-name]}))

(re-frame/reg-sub
  :nav/page
  :<- [:nav/route]
  (fn [route _]
    (-> route :data :name)))

(re-frame/reg-fx
  :nav/redirect-fx!
  (fn [url]
    (.replace (.-location js/window) url)))

(re-frame/reg-event-fx
  :nav/redirect!
  (fn [_ [_ url]]
    {:nav/redirect-fx! url}))

(re-frame/reg-event-fx
  :nav/route-changed
  (fn [{:keys [db]} [_ route]]
    (let [tid           (get-in route [:path-params :tid])
          round-id      (get-in route [:path-params :round-id])
          round-room-id (get-in route [:path-params :round-room-id])]
      {:db         (assoc db :common/active-tid tid
                             :common/public? false
                             :tournament/active-round-id round-id
                             :public.ballot/round-room-id round-room-id)
       :dispatch-n [[:kee-frame.router/route-changed route]
                    (when tid
                      [:ws/reset-ws :tournament tid])]})))