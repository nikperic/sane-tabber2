(ns sane-tabber2.ws
  (:require
    [clojure.string :as string]
    [re-frame.core :as re-frame]
    [taoensso.sente :as sente]))

(defonce response-channel (atom nil))

(defn channel-format [channel params]
  (if params
    (keyword (name channel) (str "_" (string/join "-" params)))
    channel))

(let [connection (sente/make-channel-socket! "/ws" js/csrfToken {:type :auto})]
  (def ch-chsk (:ch-recv connection))                       ; ChannelSocket's receive channel
  (def send-message! (:send-fn connection)))

(defn state-handler [{:keys [?data]}]
  (.log js/console (str "state changed: " ?data)))

(defn handshake-handler [{:keys [?data]}]
  (.log js/console (str "connection established: " ?data)))

(defn default-event-handler [ev-msg]
  (.log js/console (str "Unhandled event: " (:event ev-msg))))

(defn event-msg-handler [& [{:keys [state handshake]
                             :or   {state     state-handler
                                    handshake handshake-handler}}]]
  (fn [ev-msg]
    (case (:id ev-msg)
      :chsk/handshake (handshake ev-msg)
      :chsk/state (state ev-msg)
      :chsk/recv (when (coll? (:?data ev-msg))
                   (re-frame/dispatch (:?data ev-msg)))
      (default-event-handler ev-msg))))

(def router (atom nil))

(defn stop-router! []
  (when-let [stop-f @router] (stop-f)))

(defn start-router! []
  (stop-router!)
  (reset! router (sente/start-chsk-router!
                   ch-chsk
                   (event-msg-handler
                     {:state     state-handler
                      :handshake handshake-handler}))))

;; Re-frame events and fx

(re-frame/reg-fx
  :ws/reset-response-channel
  (fn [channel]
    (reset! response-channel channel)))

(re-frame/reg-fx
  :ws/send
  (fn [[_ data :as msg]]
    (send-message!
      (if (map? data)
        (assoc-in (vec msg) [1 :response-channel] @response-channel)
        msg)
      (fn [result]
        (when (#{:chsk/closed :chsk/error} result)
          (re-frame/dispatch [:common/notification {:type     :error
                                                    :message  "Error establishing websocket connection. Please refresh the page to continue editing."
                                                    :duration 0}]))))))

(re-frame/reg-event-fx
  :ws/reset-ws
  (fn [_ [_ page & params]]
    (let [channel (channel-format page params)]
      {:ws/reset-response-channel channel
       :ws/send                   [:comm/reset-subs channel]})))

(re-frame/reg-event-fx
  :ws/multi-ws-dispatcher
  (fn [_ [_ args]]
    {:dispatch-n args}))