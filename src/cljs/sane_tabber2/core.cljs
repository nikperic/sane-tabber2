(ns sane-tabber2.core
  (:require
    [kee-frame.core :as kf]
    [re-frame.core :as rf]
    [ajax.core :as http]
    [sane-tabber2.ajax :as ajax]
    [sane-tabber2.routing :as routing]
    [sane-tabber2.view :as view]
    [sane-tabber2.ws :as ws]

    [sane-tabber2.common.events]
    [sane-tabber2.common.subscriptions]
    [sane-tabber2.auth.events]
    [sane-tabber2.auth.subscriptions]
    [sane-tabber2.profile.events]
    [sane-tabber2.profile.subscriptions]
    [sane-tabber2.public.tournament.ballot.events]
    [sane-tabber2.public.tournament.ballot.subscriptions]
    [sane-tabber2.public.tournament.registration.events]
    [sane-tabber2.public.tournament.registration.subscriptions]
    [sane-tabber2.tournaments.create.events]
    [sane-tabber2.tournaments.create.subscriptions]
    [sane-tabber2.tournaments.events]
    [sane-tabber2.tournaments.subscriptions]
    [sane-tabber2.tournament.ballots.events]
    [sane-tabber2.tournament.ballots.subscriptions]
    [sane-tabber2.tournament.pairings.events]
    [sane-tabber2.tournament.pairings.subscriptions]
    [sane-tabber2.tournament.registration.events]
    [sane-tabber2.tournament.registration.subscriptions]
    [sane-tabber2.tournament.reporting.events]
    [sane-tabber2.tournament.reporting.subscriptions]
    [sane-tabber2.tournament.rounds.events]
    [sane-tabber2.tournament.rounds.subscriptions]

    [cljs.spec.alpha :as s]
    [expound.alpha :as expound]
    [devtools.core :as devtools]))

;; -------------------------
;; Initialize app
(defn ^:dev/after-load mount-components
  []
  (rf/clear-subscription-cache!)
  (kf/start! {:route-change-event :nav/route-changed
              :routes             routing/routes
              :hash-routing?      false
              :initial-db         {}
              :root-component     [view/root-component]}))

(defn debug-setup!
  []
  (extend-protocol IPrintWithWriter
    js/Symbol
    (-pr-writer [sym writer _]
      (-write writer (str "\"" (.toString sym) "\""))))

  (set! s/*explain-out* expound/printer)

  (enable-console-print!)

  (devtools/install!))

(defn init! []
  (when js/goog.DEBUG (debug-setup!))
  (ajax/load-interceptors!)
  (ws/start-router!)
  (mount-components))