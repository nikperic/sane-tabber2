(ns sane-tabber2.auth.events
  (:require
    [goog.crypt.base64 :as b64]
    [kee-frame.core :as kee-frame]
    [re-frame.core :as re-frame]
    [reframe-utils.core :as rf-utils]
    [sane-tabber2.common.events :as common-events]
    [sane-tabber2.validation.user :as validation]))

(rf-utils/reg-set-event :auth/loading?)

(def default-redirect "/auth")

(re-frame/reg-event-fx
  :auth/init-login
  (fn [{:keys [db]} [_ {:keys [redirect-url]}]]
    {:db (assoc db :auth/redirect (or redirect-url default-redirect))}))

(defn auth-hash [{:keys [username password]}]
  (->> password
       (str username ":")
       (b64/encodeString)
       (str "Basic ")))

(re-frame/reg-event-fx
  :auth/login-error
  (fn [_ [_ response]]
    {:dispatch-n [[:auth/set-loading? false]
                  [:common/error-handler response]]}))

(re-frame/reg-event-fx
  :auth/submit-login!
  (fn [{:keys [db]} [_ user]]
    {:reframe-utils/http
     {:method     :post
      :uri        "/public/api/login"
      :headers    {"Authorization" (auth-hash user)}
      :params     {:redirect (:auth/redirect db)}
      :on-success [:nav/redirect!]
      :on-error   [:auth/login-error]}}))

(re-frame/reg-event-fx
  :auth/login!
  (fn [{:keys [db]} [_ username password]]
    ;; prevent multiple form submission
    (if (:auth/loading? db)
      {}

      (let [user {:username username
                  :password password}]
        ;; ensure valid form
        (if-let [errors (validation/validate-user user)]
          {:dispatch-n (common-events/notify-errors errors false)}
          {:dispatch-n [[:auth/set-loading? true]
                        [:auth/submit-login! user]]})))))

(kee-frame/reg-controller
  :auth/auth-controllers
  {:params (fn [route-data]
             (when (-> route-data
                       (get-in [:data :name])
                       (= :auth))
               {:redirect-url (get-in route-data [:path-params :redirect-url])}))
   :start  [:auth/init-login]})