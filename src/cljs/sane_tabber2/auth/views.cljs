(ns sane-tabber2.auth.views
  (:require
    [reagent.core :as reagent]
    [re-frame.core :refer [dispatch subscribe]]
    [sane-tabber2.utils :as utils]
    [syn-antd.button :as button]
    [syn-antd.input :as input]))

(defn login-page []
  (let [username      (reagent/atom "")
        password      (reagent/atom "")
        loading?      (subscribe [:auth/loading?])
        submit-login! #(dispatch [:auth/login! @username @password])]
    (fn []
      [:div.login-form
       [:div.login-logo
        [:img
         {:alt "logo"
          :src "/img/logo.png"}]]
       [utils/with-focus
        [input/input
         {:value          @username
          :on-change      #(reset! username (utils/target-value %))
          :on-press-enter submit-login!
          :placeholder    "Username"}]]
       [input/input
        {:type           "password"
         :value          @password
         :on-change      #(reset! password (utils/target-value %))
         :on-press-enter submit-login!
         :placeholder    "Password"}]
       [button/button
        {:type     "primary"
         :on-click submit-login!
         :loading  (or @loading? false)
         :block    true}
        "Sign In"]])))