(ns sane-tabber2.tournament.rounds.events
  (:require
    [kee-frame.core :as kee-frame]
    [reframe-utils.core :as rf-utils]
    [re-frame.core :as re-frame]))

(rf-utils/multi-generation
  rf-utils/reg-ajax-get-event
  ["/auth/api/tournaments/%s/rounds" :tournament/rounds])

(re-frame/reg-event-fx
  :rounds/init-page
  (fn [{:keys [db]} _]
    (when-let [tid (:common/active-tid db)]
      {:dispatch [:tournament/get-rounds tid]})))

(kee-frame/reg-controller
  :rounds/rounds-controller
  {:params (fn [route-data]
             (when (-> route-data
                       (get-in [:data :name])
                       (= :tournament.rounds))
               true))
   :start  [:rounds/init-page]})

(re-frame/reg-event-fx
  :tournament/create-round
  (fn [{:keys [db]} _]
    {:ws/send [:tournament/create-round {:tid (:common/active-tid db)}]}))

(re-frame/reg-event-fx
  :tournament/auto-pair-round
  (fn [{:keys [db]} [_ tid round-id]]
    {:db      (assoc db :common/loading? true)
     :ws/send [:tournament/auto-pair-round {:tid      tid
                                            :round-id round-id}]}))

(re-frame/reg-event-fx
  :tournament/delete-round
  (fn [{:keys [db]} [_ round-id]]
    {:ws/send [:tournament/delete-round {:tid      (:common/active-tid db)
                                         :round-id round-id}]}))

(re-frame/reg-event-db
  :tournament/finished-auto-pairing
  (fn [db [_ new-round]]
    (-> db
        (assoc :common/loading? false)
        (update :tournament/rounds (fn [rounds]
                                     (map
                                       (fn [{:keys [id] :as round}]
                                         (if (= id (:id new-round))
                                           new-round
                                           round))
                                       rounds))))))

(rf-utils/reg-add-event :tournament/add-round :tournament/rounds)
(rf-utils/reg-update-by-id-event :tournament/update-round :tournament/rounds)

(re-frame/reg-event-db
  :tournament/remove-round
  (fn [db [_ round-id]]
    (update db :tournament/rounds
            (fn [rounds]
              (remove #(= round-id (:id %))
                      rounds)))))

(re-frame/reg-event-fx
  :tournament/delete-judge-emails
  (fn [_ [_ tid]]
    {:reframe-utils/http {:method     :delete
                          :uri        (str "/auth/api/tournaments/" tid "/judges/emails")
                          :on-error   [:common/message {:type    :error
                                                        :message "Unable to delete judge emails"}]
                          :on-success [:common/message {:type :success
                                                        :message "Emails have been cleared!"}]}}))