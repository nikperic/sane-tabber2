(ns sane-tabber2.tournament.rounds.views
  (:require
    [kee-frame.core :as kee-frame]
    [reagent.core :as reagent]
    [re-frame.core :as re-frame]
    [syn-antd.button :as button]
    [syn-antd.divider :as divider]
    [syn-antd.list :as list]
    [syn-antd.space :as space]
    [syn-antd.tag :as tag]))

(defn rounds-page
  []
  (let [tid    (re-frame/subscribe [:common/active-tid])
        rounds (re-frame/subscribe [:tournament/rounds])]
    (fn []
      [space/space
       {:direction "vertical"
        :style     {:width "100%"}}
       [button/button
        {:href   (kee-frame/path-for [:public.tournament.registration-view {:tid @tid}])
         :target "_blank"}
        "Registration Review (public link)"]
       [list/list
        {:dataSource
         (sort-by :round-number @rounds)

         :bordered
         true

         :render-item
         (fn [item]
           (let [status   (aget item "status")
                 round-id (aget item "id")
                 paired?  (= "paired" status)]
             (reagent/as-element
               [list/list-item
                {:actions (filter identity
                                  [(when (not paired?)
                                     (reagent/as-element
                                       [:a
                                        {:on-click #(re-frame/dispatch [:tournament/auto-pair-round @tid round-id])}
                                        "Auto Pair"]))
                                   (when paired?
                                     (reagent/as-element
                                       [:a
                                        {:href (kee-frame/path-for [:tournament.pairings
                                                                    {:tid      @tid
                                                                     :round-id round-id}])}
                                        "View Rooms"]))
                                   (when paired?
                                     (reagent/as-element
                                       [:a
                                        {:href (kee-frame/path-for [:tournament.ballots
                                                                    {:tid      @tid
                                                                     :round-id round-id}])}
                                        "Enter Ballots"]))
                                   (when paired?
                                     (reagent/as-element
                                       [:a
                                        {:on-click #(when (js/confirm "Are you sure you wish to send all judges emails")
                                                      (.open js/window (str "/auth/api/tournaments/" @tid "/rounds/" round-id "/rooms/mail-judges") "_blank"))}
                                        "Send Judge Emails"]))
                                   (reagent/as-element
                                     [:a
                                      {:on-click #(when (js/confirm "Are you sure you wish to delete this round?")
                                                    (re-frame/dispatch [:tournament/delete-round round-id]))}
                                      "Delete"])])}
                [:span "Round " (aget item "round-number")]
                [tag/tag
                 {:color (case status
                           "paired" "green"
                           "red")}
                 status]])))

         :style
         {:max-width "640px"}}]
       [button/button
        {:type     :primary
         :on-click #(re-frame/dispatch [:tournament/create-round])}
        "Create round"]
       [divider/divider]
       [:p "This is the danger zone :o"]
       [button/button
        {:type "danger"
         :on-click #(when (js/confirm "Are you sure you wish to delete ALL the judge emails from the database? This is final and can't be undone")
                      (when (js/confirm "Last warning, about to delete ALL judge emails for this tournament. Please confirm or cancel.")
                        (re-frame/dispatch [:tournament/delete-judge-emails @tid])))}
        "Remove Judge Emails"]])))