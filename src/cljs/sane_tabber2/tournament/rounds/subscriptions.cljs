(ns sane-tabber2.tournament.rounds.subscriptions
  (:require
    [reframe-utils.core :as rf-utils]))

(rf-utils/multi-generation
  rf-utils/reg-basic-sub
  :tournament/rounds)