(ns sane-tabber2.tournament.reporting.views
  (:require
    [re-frame.core :as re-frame]
    [syn-antd.button :as button]
    [syn-antd.space :as space]))

(defn report-button
  [href label]
  [button/button
   {:block  true
    :href   href
    :target "_blank"}
   label])

(defn reporting-page
  []
  (let [tid (re-frame/subscribe [:common/active-tid])]
    (fn []
      (let [base-url (str "/auth/api/tournaments/" @tid "/reports/")]
        [space/space
         {:direction "vertical"
          :style     {:width "100%"}}
         [report-button (str base-url "team-tab") "Team Tab"]
         [report-button (str base-url "speaker-tab") "Speaker Tab"]
         [report-button (str base-url "team-stats") "Team Stats"]
         [report-button (str base-url "teams") "Teams"]
         [report-button (str base-url "judges") "Judges"]
         [report-button (str base-url "rooms") "Rooms"]
         [report-button (str base-url "schools") "Schools"]]))))