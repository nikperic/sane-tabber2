(ns sane-tabber2.tournament.registration.subscriptions
  (:require
    [reframe-utils.core :as rf-utils]
    [re-frame.core :as re-frame]
    [sane-tabber2.common :as common]))

(rf-utils/multi-generation
  rf-utils/reg-basic-sub
  :tournament/judges
  :tournament/rooms
  :tournament/schools
  :tournament/teams
  :registration/active-tab
  :registration.modals/new-team-visible?)

(re-frame/reg-sub
  :tournament/teams-with-school-names
  :<- [:tournament/teams]
  :<- [:tournament/schools]
  (fn [[teams schools] _]
    (sort-by
      (juxt :school-code :team-code)
      (map (fn [{:keys [school-id] :as team}]
             (let [school (common/filter-first #(= school-id (:id %)) schools)]
               (assoc team :school-code (:code school)
                           :school-name (:name school))))
           teams))))

(re-frame/reg-sub
  :tournament/signed-in-teams
  :<- [:tournament/teams-with-school-names]
  (fn [teams _]
    (filter :signed-in? teams)))

(re-frame/reg-sub
  :tournament/signed-in-judges
  :<- [:tournament/judges]
  (fn [judges _]
    (sort-by :name (filter :signed-in? judges))))

(re-frame/reg-sub
  :tournament/in-use-rooms
  :<- [:tournament/rooms]
  (fn [rooms _]
    (sort-by :name (filter :in-use? rooms))))

(defn school-filter
  [schools kw]
  (sort-by :value (map (fn [school]
                         (let [v (get school kw)]
                           {:value v
                            :text  v}))
                       schools)))

(re-frame/reg-sub
  :registration/school-codes
  :<- [:tournament/schools]
  (fn [schools _]
    (school-filter schools :code)))

(re-frame/reg-sub
  :registration/school-names
  :<- [:tournament/schools]
  (fn [schools _]
    (school-filter schools :name)))

(re-frame/reg-sub
  :registration/last-team-code
  :<- [:tournament/teams]
  (fn [teams [_ school-id]]
    (->> teams
         (filter #(= school-id (:school-id %)))
         (sort-by :team-code)
         (last)
         :team-code)))