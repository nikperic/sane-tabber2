(ns sane-tabber2.tournament.registration.events
  (:require
    [kee-frame.core :as kee-frame]
    [reframe-utils.core :as rf-utils]
    [re-frame.core :as re-frame]
    [sane-tabber2.common :as common]))

(rf-utils/multi-generation
  rf-utils/reg-ajax-get-event
  ["/auth/api/tournaments/%s/judges" :tournament/judges]
  ["/auth/api/tournaments/%s/rooms" :tournament/rooms]
  ["/auth/api/tournaments/%s/schools" :tournament/schools]
  ["/auth/api/tournaments/%s/teams" :tournament/teams])

(rf-utils/multi-generation
  rf-utils/reg-set-event
  :registration/active-tab
  :registration.modals/new-team-visible?)

(rf-utils/multi-generation
  rf-utils/reg-add-event
  [:tournament/add-judge :tournament/judges]
  [:tournament/add-room :tournament/rooms]
  [:tournament/add-school :tournament/schools]
  [:tournament/add-team :tournament/teams])

(rf-utils/multi-generation
  rf-utils/reg-update-by-id-event
  [:tournament/update-judge :tournament/judges]
  [:tournament/update-room :tournament/rooms]
  [:tournament/update-school :tournament/schools]
  [:tournament/update-team :tournament/teams])

(re-frame/reg-event-fx
  :registration/init-page
  (fn [{:keys [db]} _]
    (let [tid (:common/active-tid db)]
      {:db         (assoc db :registration/active-tab "teams")
       :dispatch-n [[:tournament/get-judges tid]
                    [:tournament/get-rooms tid]
                    [:tournament/get-schools tid]
                    [:tournament/get-teams tid]]})))

(kee-frame/reg-controller
  :registration/registration-controller
  {:params (fn [route-data]
             (when (-> route-data
                       (get-in [:data :name])
                       (= :tournament.registration))
               true))
   :start  [:registration/init-page]})


;; TEAMS
(re-frame/reg-event-fx
  :registration/create-team
  (fn [_ [_ new-team]]
    {:ws/send [:tournament/create-team new-team]}))

(re-frame/reg-event-fx
  :registration/update-team-school
  (fn [{:keys [db]} [_ team-id new-school-id]]
    (let [teams (:tournament/teams db)]
      {:ws/send [:tournament/update-team (assoc (common/filter-first #(= team-id (:id %)) teams)
                                           :school-id new-school-id
                                           :tid (:common/active-tid db))]})))

(re-frame/reg-event-fx
  :registration/update-team-key
  (fn [{:keys [db]} [_ team-id kw new-value]]
    {:ws/send [:tournament/update-team (assoc (common/filter-first #(= team-id (:id %)) (:tournament/teams db))
                                         kw new-value
                                         :tid (:common/active-tid db))]}))

;; JUDGES
(re-frame/reg-event-fx
  :registration/create-judge
  (fn [_ [_ new-judge]]
    {:ws/send [:tournament/create-judge new-judge]}))

(re-frame/reg-event-fx
  :registration/update-judge-key
  (fn [{:keys [db]} [_ judge-id kw new-value]]
    {:ws/send [:tournament/update-judge (assoc (common/filter-first #(= judge-id (:id %)) (:tournament/judges db))
                                          kw new-value
                                          :tid (:common/active-tid db))]}))

;; ROOMS

(re-frame/reg-event-fx
  :registration/create-room
  (fn [_ [_ new-room]]
    {:ws/send [:tournament/create-room new-room]}))

(re-frame/reg-event-fx
  :registration/update-room-key
  (fn [{:keys [db]} [_ room-id kw new-value]]
    {:ws/send [:tournament/update-room (assoc (common/filter-first #(= room-id (:id %)) (:tournament/rooms db))
                                         kw new-value
                                         :tid (:common/active-tid db))]}))


;; SCHOOLS

(re-frame/reg-event-fx
  :registration/create-school
  (fn [_ [_ new-school]]
    {:ws/send [:tournament/create-school new-school]}))

(re-frame/reg-event-fx
  :registration/update-school-key
  (fn [{:keys [db]} [_ school-id kw new-value]]
    {:ws/send [:tournament/update-school (assoc (common/filter-first #(= school-id (:id %)) (:tournament/schools db))
                                         kw new-value
                                         :tid (:common/active-tid db))]}))