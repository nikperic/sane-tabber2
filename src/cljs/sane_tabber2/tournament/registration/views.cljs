(ns sane-tabber2.tournament.registration.views
  (:require
    [clojure.string :as string]
    [reagent.core :as reagent]
    [reagent.dom :as reagent-dom]
    [re-frame.core :as re-frame]
    [sane-tabber2.utils :as utils]
    [syn-antd.button :as button]
    [syn-antd.form :as form]
    [syn-antd.input :as input]
    [syn-antd.modal :as modal]
    [syn-antd.select :as select]
    [syn-antd.space :as space]
    [syn-antd.table :as table]
    [syn-antd.tabs :as tabs]
    [syn-antd.tooltip :as tooltip]))

(defn signed-in-sorter
  [a b]
  (- (if (aget a "signed-in?") 1 0)
     (if (aget b "signed-in?") 1 0)))

(defn in-use-sorter
  [a b]
  (- (if (aget a "in-use?") 1 0)
     (if (aget b "in-use?") 1 0)))

(defn default-filter-fn
  [target-column]
  (fn [value record]
    (> (.indexOf (string/lower-case (aget record (name target-column)))
                 (string/lower-case value))
       -1)))

(defn text-filter-box
  [filter-text label]
  [tooltip/tooltip
   {:title     (str "Looking for a " label "? Search for their name here and hit enter or press the search button")
    :placement "topLeft"}
   [:div
    [input/input-search
     {:placeholder  (str "Search for " label "...")
      :value        @filter-text
      :allow-clear  true
      :enter-button (not (string/blank? @filter-text))
      :on-search    #(reset! filter-text %1)
      :on-key-up    (fn [e]
                      (when (= 27 (.-keyCode e))
                        (reset! filter-text "")))}]]])

(defn editable-table-cell
  [value on-save & [renderer]]
  (let [editing?  (reagent/atom false)
        input-ref (atom nil)]
    (fn [value on-save & [renderer]]
      [:<>
       (let [save! (fn []
                     (let [new-value (.-value @input-ref)]
                       (when (and (not= new-value value)
                                  (not (string/blank? new-value)))
                         (on-save new-value)))
                     (reset! editing? false))]
         [input/input
          {:ref            (fn [node] (reset! input-ref (reagent-dom/dom-node node)))
           :on-blur        save!
           :on-press-enter save!
           :on-key-up      (fn [e]
                             (when (= 27 (.-keyCode e))
                               (reset! editing? false)))
           :value          value
           :style          {:display (if @editing? "block" "none")}}])

       [:div.editable-cell-value-wrap
        {:on-click (fn []
                     (reset! editing? true)
                     (js/setTimeout (fn []
                                      (.focus @input-ref)
                                      (.select @input-ref)) 100))
         :style    {:display (if @editing? "none" "block")}}
        (or renderer value)]])))

(defn school-selection
  [record]
  (let [schools (re-frame/subscribe [:tournament/schools])]
    (fn [record]
      [select/select
       {:value              (aget record "school-id")
        :on-change          #(re-frame/dispatch [:registration/update-team-school (aget record "id") %])
        :show-search        true
        :option-filter-prop "children"
        :style              {:width "100%"}}
       (select/ant-options {:options  (sort-by :code @schools)
                            :id-fn    :id
                            :label-fn :name})])))

(defn speaker-props
  [n]
  (let [data-index (str "speaker" n)]
    {:title     (str "Speaker " n)
     :dataIndex data-index
     :render    (fn [value record]
                  (reagent/as-element
                    [editable-table-cell
                     value
                     #(re-frame/dispatch [:registration/update-team-key (aget record "id") (keyword data-index) %])]))}))

(def base-new-team
  {:team-code   "A"
   :school-id   nil
   :speaker1    ""
   :speaker2    ""
   :signed-in?  false
   :accessible? false})

(defn speaker-input
  [team-atom speaker-n]
  (let [speaker-label (str "Speaker " speaker-n)
        speaker-kw    (keyword (str "speaker" speaker-n))]
    [form/form-item
     {:label speaker-label}
     [input/input
      {:value     (get @team-atom speaker-kw)
       :on-change #(swap! team-atom assoc speaker-kw (utils/target-value %))}]]))

(defn new-team-modal
  [tid]
  (let [visible?     (re-frame/subscribe [:registration.modals/new-team-visible?])
        schools      (re-frame/subscribe [:tournament/schools])
        new-team     (reagent/atom base-new-team)
        close-modal! (fn []
                       (reset! new-team base-new-team)
                       (re-frame/dispatch [:registration.modals/set-new-team-visible? false]))]
    (fn [tid]
      [modal/modal
       {:visible   @visible?
        ;:width     "75%"
        :title     "Create Team"
        :ok-text   "Create Team"
        :on-cancel close-modal!
        :on-ok     (fn []
                     (re-frame/dispatch [:registration/create-team (assoc @new-team :tournament-id tid :tid tid)])
                     (close-modal!))}

       [form/form-item
        {:label "School"}
        [select/select
         {:value              (:school-id @new-team)
          :on-change          #(swap! new-team assoc :school-id %)
          :show-search        true
          :option-filter-prop "children"}
         (select/ant-options {:options  (sort-by :code @schools)
                              :label-fn (fn [{:keys [code name]}] (str name " [" code "]"))
                              :id-fn    :id})]]
       [form/form-item
        {:label "Team Code"
         :extra (when-some [school-id (:school-id @new-team)]
                  (let [last-team-code (re-frame/subscribe [:registration/last-team-code school-id])]
                    (reagent/as-element
                      [:p {:color "#262626"}
                       "Last team code used for this school: " (or @last-team-code "<none>")])))}
        [input/input
         {:value     (:team-code @new-team)
          :on-change #(swap! new-team assoc :team-code (utils/target-value %))}]]
       [speaker-input new-team 1]
       [speaker-input new-team 2]])))

(defn registration-modals
  [tid]
  [:<>
   [new-team-modal tid]])

(defn teams-editor
  [tid]
  (let [teams        (re-frame/subscribe [:tournament/teams-with-school-names])
        school-codes (re-frame/subscribe [:registration/school-codes])
        school-names (re-frame/subscribe [:registration/school-names])
        filter-text  (reagent/atom "")]
    (fn [tid]
      [:div
       [:p "Signed in Teams: " (count (filter :signed-in? @teams))]
       [table/table
        {:bordered
         true

         :dataSource
         @teams

         ;:pagination {:defaultPageSize 2}

         :row-class-name
         "editable-row"

         :title
         (fn []
           (reagent/as-element
             [text-filter-box filter-text "speaker"]))

         :footer
         (fn []
           (reagent/as-element
             [button/button
              {:type     "primary"
               :on-click #(re-frame/dispatch [:registration.modals/set-new-team-visible? true])}
              "Create New Team"]))

         :columns
         [{:title     "School Code"
           :dataIndex "school-code"
           ;:filters   @school-codes
           :onFilter  (default-filter-fn :school-code)
           :width     "100px"}
          {:title     "School"
           :dataIndex "school-name"
           ;; TODO: figure out why can't have multiple filters when one is custom? Seems dumb
           ;:filters   @school-names
           :onFilter  (default-filter-fn :school-name)
           :render    (fn [_ record]
                        (reagent/as-element [school-selection record]))}
          {:title     "Team Code"
           :dataIndex "team-code"
           :width     "100px"
           :render    (fn [value record]
                        (reagent/as-element
                          [editable-table-cell
                           value
                           #(re-frame/dispatch [:registration/update-team-key (aget record "id") :team-code %])]))}
          ;; TODO: allow customizable # of speakers
          (merge (speaker-props 1)
                 {:filtered      (not (string/blank? @filter-text))
                  :filteredValue (when-not (string/blank? @filter-text) [@filter-text])
                  :onFilter      (fn [value record]
                                   (or ((default-filter-fn :speaker1) value record)
                                       ((default-filter-fn :speaker2) value record)))})
          (speaker-props 2)

          {:title     "Actions"
           :dataIndex "actions"
           :sorter    signed-in-sorter
           :render    (fn [_ record]
                        (let [signed-in?  (aget record "signed-in?")
                              accessible? (aget record "accessible?")
                              team-id     (aget record "id")]
                          (reagent/as-element
                            [space/space
                             (if signed-in?
                               [button/button
                                {:type     "primary"
                                 :on-click #(re-frame/dispatch [:registration/update-team-key team-id :signed-in? false])}
                                "Signed In"]
                               [button/button
                                {:type     "danger"
                                 :on-click #(re-frame/dispatch [:registration/update-team-key team-id :signed-in? true])}
                                "NOT SIGNED IN"])
                             (if accessible?
                               [button/button
                                {:style    {:background-color "#87e8de"
                                            :color            "#262626"}
                                 :on-click #(re-frame/dispatch [:registration/update-team-key team-id :accessible? false])}
                                "Accessible"]
                               [button/button
                                {:on-click #(re-frame/dispatch [:registration/update-team-key team-id :accessible? true])}
                                "Not Accessible"])])))}]}]])))

(def base-new-judge
  {:name        ""
   :email       ""
   :rating      5
   :signed-in?  false
   :accessible? false})

(defn new-judge-node
  [tid]
  (let [new-judge (reagent/atom base-new-judge)
        reset-fn  #(reset! new-judge base-new-judge)]
    (fn [tid]
      [:div
       [:h4 "Create new Judge"]
       [form/form
        {:layout "inline"}
        [form/form-item
         {:label "Name"}
         [input/input
          {:value     (:name @new-judge)
           :on-change #(swap! new-judge assoc :name (utils/target-value %))}]]
        [form/form-item
         {:label "Email"}
         [input/input
          {:value     (:email @new-judge)
           :on-change #(swap! new-judge assoc :email (utils/target-value %))}]]
        [button/button
         {:type     "primary"
          :on-click (fn []
                      ;; TODO: validate
                      (re-frame/dispatch [:registration/create-judge (assoc @new-judge :tournament-id tid
                                                                                       :tid tid)])
                      (reset-fn))}
         "Create"]
        [button/button
         {:on-click reset-fn}
         "Clear"]]])))

;; TODO
(defn scratches-editor
  []
  )

;; TODO: add scratches
(defn judges-editor
  [tid]
  (let [judges      (re-frame/subscribe [:tournament/judges])
        filter-text (reagent/atom "")]
    (fn [tid]
      [:div
       [:p "Signed in Judges: " (count (filter :signed-in? @judges))]
       [table/table
        {:bordered
         true

         :dataSource
         (sort-by :name @judges)

         :row-class-name
         "editable-row"

         :title
         (fn []
           (reagent/as-element [text-filter-box filter-text "judge"]))

         :footer
         (fn [] (reagent/as-element [new-judge-node tid]))

         :columns
         [
          {:title         "Name"
           :dataIndex     "name"
           :filtered      (not (string/blank? @filter-text))
           :filteredValue (when-not (string/blank? @filter-text) [@filter-text])
           :onFilter      (default-filter-fn :name)
           :render        (fn [value record]
                            (reagent/as-element
                              [editable-table-cell
                               value
                               #(re-frame/dispatch [:registration/update-judge-key (aget record "id") :name %])]))}
          {:title     "Rating"
           :dataIndex "rating"
           :render    (fn [value record]
                        (reagent/as-element
                          [select/select
                           {:value              (aget record "rating")
                            :on-change          #(re-frame/dispatch [:registration/update-judge-key (aget record "id") :rating %])
                            :show-search        true
                            :option-filter-prop "children"
                            :style              {:width "100%"}}
                           (select/ant-options {:options (map (fn [i]
                                                                (let [n (inc i)]
                                                                  {:id    n
                                                                   :label (str n)}))
                                                              (range 10))})]))}
          {:title     "Email"
           :dataIndex "email"
           :render    (fn [value record]
                        (reagent/as-element
                          [editable-table-cell
                           value
                           #(re-frame/dispatch [:registration/update-judge-key (aget record "id") :email %])]))}
          {:title     "Actions"
           :dataIndex "id"
           :sorter    signed-in-sorter
           :render    (fn [judge-id record]
                        (let [signed-in?  (aget record "signed-in?")
                              accessible? (aget record "accessible?")]
                          (reagent/as-element
                            [space/space
                             (if signed-in?
                               [button/button
                                {:type     "primary"
                                 :on-click #(re-frame/dispatch [:registration/update-judge-key judge-id :signed-in? false])}
                                "Signed In"]
                               [button/button
                                {:type     "danger"
                                 :on-click #(re-frame/dispatch [:registration/update-judge-key judge-id :signed-in? true])}
                                "NOT SIGNED IN"])
                             (if accessible?
                               [button/button
                                {:style    {:background-color "#87e8de"
                                            :color            "#262626"}
                                 :on-click #(re-frame/dispatch [:registration/update-judge-key judge-id :accessible? false])}
                                "Accessible"]
                               [button/button
                                {:on-click #(re-frame/dispatch [:registration/update-judge-key judge-id :accessible? true])}
                                "Not Accessible"])
                             #_[button/button
                                {:on-click #()}
                                "Scratches (" (count (aget record "scratches")) ")"]])))}]}]])))

(def base-new-room {:name        ""
                    :in-use?     true
                    :accessible? false})

(defn new-room-node
  [tid]
  (let [new-room     (reagent/atom base-new-room)
        reset-fn     #(reset! new-room base-new-room)
        create-room! (fn []
                       ;; TODO: validate
                       (re-frame/dispatch [:registration/create-room (assoc @new-room :tournament-id tid
                                                                                      :tid tid)])
                       (reset-fn))]
    (fn [tid]
      [:div
       [:h4 "Create new Room"]
       [form/form
        {:layout "inline"}
        [form/form-item
         {:label "Name"}
         [input/input
          {:value          (:name @new-room)
           :on-change      #(swap! new-room assoc :name (utils/target-value %))
           :on-press-enter create-room!}]]
        [button/button
         {:type     "primary"
          :on-click create-room!}
         "Create"]
        [button/button
         {:on-click reset-fn}
         "Clear"]]])))

(defn rooms-editor
  [tid]
  (let [rooms       (re-frame/subscribe [:tournament/rooms])
        filter-text (reagent/atom "")]
    (fn [tid]
      [:div
       [:p "In use Rooms: " (count (filter :in-use? @rooms))]
       [table/table
        {:bordered
         true

         :dataSource
         (sort-by :name @rooms)

         :row-class-name
         "editable-row"

         :title
         (fn []
           (reagent/as-element [text-filter-box filter-text "room"]))

         :footer
         (fn [] (reagent/as-element [new-room-node tid]))

         :columns
         [
          {:title         "Name"
           :dataIndex     "name"
           :filtered      (not (string/blank? @filter-text))
           :filteredValue (when-not (string/blank? @filter-text) [@filter-text])
           :onFilter      (default-filter-fn :name)
           :render        (fn [value record]
                            (reagent/as-element
                              [editable-table-cell
                               value
                               #(re-frame/dispatch [:registration/update-room-key (aget record "id") :name %])]))}
          {:title     "Actions"
           :dataIndex "actions"
           :sorter    in-use-sorter
           :render    (fn [_ record]
                        (let [in-use?     (aget record "in-use?")
                              accessible? (aget record "accessible?")
                              room-id     (aget record "id")]
                          (reagent/as-element
                            [space/space
                             (if in-use?
                               [button/button
                                {:type     "primary"
                                 :on-click #(re-frame/dispatch [:registration/update-room-key room-id :in-use? false])}
                                "IN USE"]
                               [button/button
                                {:type     "danger"
                                 :on-click #(re-frame/dispatch [:registration/update-room-key room-id :in-use? true])}
                                "NOT IN USE"])
                             (if accessible?
                               [button/button
                                {:style    {:background-color "#87e8de"
                                            :color            "#262626"}
                                 :on-click #(re-frame/dispatch [:registration/update-room-key room-id :accessible? false])}
                                "Accessible"]
                               [button/button
                                {:on-click #(re-frame/dispatch [:registration/update-room-key room-id :accessible? true])}
                                "Not Accessible"])])))}]}]])))

(def base-new-school
  {:name ""
   :code ""})

(defn new-school-node
  [tid]
  (let [new-school (reagent/atom base-new-school)
        reset-fn   #(reset! new-school base-new-school)]
    (fn [tid]
      [:div
       [:h4 "Create new School"]
       [form/form
        {:layout "inline"}
        [form/form-item
         {:label "Name"}
         [input/input
          {:value     (:name @new-school)
           :on-change #(swap! new-school assoc :name (utils/target-value %))}]]
        [form/form-item
         {:label "School Code"}
         [input/input
          {:value     (:code @new-school)
           :on-change #(swap! new-school assoc :code (utils/target-value %))}]]
        [button/button
         {:type     "primary"
          :on-click (fn []
                      ;; TODO: validate
                      (re-frame/dispatch [:registration/create-school (assoc @new-school :tournament-id tid
                                                                                         :tid tid)])
                      (reset-fn))}
         "Create"]
        [button/button
         {:on-click reset-fn}
         "Clear"]]])))

(defn schools-editor
  [tid]
  (let [schools     (re-frame/subscribe [:tournament/schools])
        filter-text (reagent/atom "")]
    (fn [tid]
      [table/table
       {:bordered
        true

        :dataSource
        (sort-by :code @schools)

        :row-class-name
        "editable-row"

        :title
        (fn []
          (reagent/as-element [text-filter-box filter-text "school"]))

        :footer
        (fn [] (reagent/as-element [new-school-node tid]))

        :columns
        [
         {:title     "School Code"
          :dataIndex "code"
          :render    (fn [value record]
                       (reagent/as-element
                         [editable-table-cell
                          value
                          #(re-frame/dispatch [:registration/update-school-key (aget record "id") :code %])]))}
         {:title         "Name"
          :dataIndex     "name"
          :filtered      (not (string/blank? @filter-text))
          :filteredValue (when-not (string/blank? @filter-text) [@filter-text])
          :onFilter      (default-filter-fn :name)
          :render        (fn [value record]
                           (reagent/as-element
                             [editable-table-cell
                              value
                              #(re-frame/dispatch [:registration/update-school-key (aget record "id") :name %])]))}]}])))

(defn registration-page
  []
  (let [tid        (re-frame/subscribe [:common/active-tid])
        active-tab (re-frame/subscribe [:registration/active-tab])]
    (fn []
      [:<>
       [registration-modals @tid]
       [tabs/tabs
        {:active-key @active-tab
         :on-change  #(re-frame/dispatch [:registration/set-active-tab %])}
        [tabs/tabs-tab-pane
         {:tab "Teams"
          :key "teams"}
         [teams-editor @tid]]
        [tabs/tabs-tab-pane
         {:tab "Judges"
          :key "judges"}
         [judges-editor @tid]]
        [tabs/tabs-tab-pane
         {:tab "Rooms"
          :key "rooms"}
         [rooms-editor @tid]]
        [tabs/tabs-tab-pane
         {:tab "Schools"
          :key "schools"}
         [schools-editor @tid]]]])))