(ns sane-tabber2.tournament.pairings.subscriptions
  (:require
    [reframe-utils.core :as rf-utils]
    [re-frame.core :as re-frame]
    [sane-tabber2.common :as common]))

(rf-utils/multi-generation
  rf-utils/reg-basic-sub
  :tournament/active-round-id
  :tournament/team-stats
  :round/round-rooms)

(re-frame/reg-sub
  :pairings/free-rooms
  :<- [:round/round-rooms]
  :<- [:tournament/in-use-rooms]
  (fn [[round-rooms in-use-rooms] _]
    (let [rooms (set (map :room round-rooms))]
      (filter (fn [{:keys [id]}]
                (not (contains? rooms id)))
              in-use-rooms))))

(re-frame/reg-sub
  :pairings/free-judges
  :<- [:round/round-rooms]
  :<- [:tournament/signed-in-judges]
  (fn [[round-rooms signed-in-judges] _]
    (let [judges (set (mapcat :judges round-rooms))]
      (filter (fn [{:keys [id]}]
                (not (contains? judges id)))
              signed-in-judges))))

(re-frame/reg-sub
  :pairings/free-teams
  :<- [:round/round-rooms]
  :<- [:tournament/signed-in-teams]
  (fn [[round-rooms signed-in-teams] _]
    (let [teams (set (mapcat (fn [{:keys [teams]}]
                               (map name (keys teams)))
                             round-rooms))]
      (filter (fn [{:keys [id]}]
                (not (contains? teams id)))
              signed-in-teams))))

(re-frame/reg-sub
  :pairings/room-dropdown
  :<- [:pairings/free-rooms]
  :<- [:tournament/in-use-rooms]
  (fn [[free-rooms all-rooms] [_ room-id]]
    (distinct (conj free-rooms
                    (common/get-first-by-id all-rooms room-id)))))

(re-frame/reg-sub
  :pairings/judge-dropdown
  :<- [:pairings/free-judges]
  :<- [:tournament/signed-in-judges]
  (fn [[free-judges all-judges] [_ judge-ids]]
    (let [judge-ids-set (set judge-ids)]
      (distinct (concat free-judges
                        (filter (fn [{:keys [id]}]
                                  (contains? judge-ids-set id))
                                all-judges))))))

(re-frame/reg-sub
  :pairings/team-dropdown
  :<- [:pairings/free-teams]
  :<- [:tournament/signed-in-teams]
  :<- [:tournament/team-stats]
  (fn [[free-teams all-teams team-stats] [_ team-id]]
    (map (fn [{:keys [id] :as team}]
           (let [{:keys [points position-data]} (common/get-first-by-id team-stats id)]
             (assoc team :points points
                         :positions position-data)))
         (distinct (conj free-teams
                         (common/get-first-by-id all-teams team-id))))))