(ns sane-tabber2.tournament.pairings.events
  (:require
    [kee-frame.core :as kee-frame]
    [re-frame.core :as re-frame]
    [reframe-utils.core :as rf-utils]))

(rf-utils/multi-generation
  rf-utils/reg-ajax-get-event
  ["/auth/api/tournaments/%s/teams/stats" :tournament/team-stats]
  ["/auth/api/tournaments/%s/rounds/%s/rooms" :round/round-rooms])

(rf-utils/multi-generation
  rf-utils/reg-add-event
  [:round/add-round-room :round/round-rooms])

(rf-utils/multi-generation
  rf-utils/reg-update-by-id-event
  [:round/update-round-room :round/round-rooms])

(re-frame/reg-event-fx
  :pairings/init-page
  (fn [{:keys [db]} _]
    (let [tid      (:common/active-tid db)
          round-id (:tournament/active-round-id db)]
      {:dispatch-n [[:tournament/get-judges tid]
                    [:tournament/get-rooms tid]
                    [:tournament/get-schools tid]
                    [:tournament/get-teams tid]
                    [:tournament/get-team-stats tid]
                    [:round/get-round-rooms tid round-id]]})))

(kee-frame/reg-controller
  :pairings/pairings-controller
  {:params (fn [route-data]
             (when (-> route-data
                       (get-in [:data :name])
                       (= :tournament.pairings))
               true))
   :start  [:pairings/init-page]})

(re-frame/reg-event-fx
  :pairings/update-round-room
  (fn [{:keys [db]} [_ new-round-room]]
    {:ws/send [:round/update-round-room (assoc new-round-room :tid (:common/active-tid db))]}))

(re-frame/reg-event-fx
  :pairings/create-round-room
  (fn [_ [_ new-round-room]]
    {:ws/send [:round/create-round-room new-round-room]}))
