(ns sane-tabber2.tournament.pairings.views
  (:require
    [clojure.set :as set]
    [kee-frame.core :as kee-frame]
    [reagent.core :as reagent]
    [re-frame.core :as re-frame]
    [sane-tabber2.utils :as utils]
    [syn-antd.button :as button]
    [syn-antd.space :as space]
    [syn-antd.table :as table]
    [syn-antd.select :as select]
    [syn-antd.form :as form]))

(defn team-name-fn
  [{:keys [school-code team-code points positions]
    :or   {points 0}}]
  (if (some? team-code)
    (str school-code " " team-code "(" points ") - [pos1: " (get positions 1 0) ", pos2: " (get positions 2 0) "]")
    "<NO TEAM>"))

(defn room-selection
  [{:keys [room] :as round-room}]
  (let [room-dropdown (re-frame/subscribe [:pairings/room-dropdown room])]
    [select/select
     {:value              room
      :on-change          #(re-frame/dispatch [:pairings/update-round-room (assoc round-room :room %)])
      :show-search        true
      :option-filter-prop "children"
      :style              {:width "100%"}}
     (select/ant-options {:options  @room-dropdown
                          :label-fn :name})]))

(defn judge-selection
  [{:keys [judges] :as round-room}]
  (let [judge-dropdown (re-frame/subscribe [:pairings/judge-dropdown judges])]
    [select/select
     {:value              judges
      :mode               "multiple"
      :on-change          #(re-frame/dispatch [:pairings/update-round-room (assoc round-room :judges (js->clj %))])
      :show-search        true
      :option-filter-prop "children"
      :style              {:width "100%"}}
     (select/ant-options {:options  @judge-dropdown
                          :label-fn :name})]))

(defn team-selection
  [{:keys [teams] :as round-room} team-n]
  (let [team-id       (-> teams
                          (set/map-invert)
                          (get team-n)
                          (utils/safe-name))
        team-dropdown (re-frame/subscribe [:pairings/team-dropdown team-id])]
    [select/select
     {:value              team-id
      :on-change          #(re-frame/dispatch [:pairings/update-round-room (update round-room :teams
                                                                                   (fn [teams]
                                                                                     (cond-> teams
                                                                                             true (dissoc (keyword team-id))
                                                                                             (some? %) (assoc (keyword %) team-n))))])
      :show-search        true
      :allow-clear        true
      :option-filter-prop "children"
      :style              {:width "100%"}}
     (select/ant-options {:options  @team-dropdown
                          :label-fn team-name-fn})]))

(def base-new-round-room
  {:room   nil
   :judges []
   :teams  {}})

;; TODO: generic team count
(defn new-round-room-form
  [tid round-id]
  (let [free-rooms     (re-frame/subscribe [:pairings/free-rooms])
        free-judges    (re-frame/subscribe [:pairings/free-judges])
        free-teams     (re-frame/subscribe [:pairings/free-teams])
        new-round-room (reagent/atom base-new-round-room)
        reset-fn       #(reset! new-round-room base-new-round-room)]
    (fn [tid round-id]
      (let [inverted-team-map (set/map-invert (:teams @new-round-room))
            team-1            (get inverted-team-map 1)
            team-2            (get inverted-team-map 2)]
        [:div
         [:h4 "New Round Room"]
         [form/form
          {:layout "inline"}
          [form/form-item
           {:label "Room"}
           [select/select
            {:value              (:room @new-round-room)
             :on-change          #(swap! new-round-room assoc :room %)
             :show-search        true
             :option-filter-prop "children"
             :style              {:min-width "128px"}}
            (select/ant-options {:options  @free-rooms
                                 :label-fn :name})]]
          [form/form-item
           {:label "Team 1"}
           [select/select
            {:value              (utils/safe-name team-1)
             :on-change          #(swap! new-round-room update :teams
                                         (fn [teams]
                                           (-> teams
                                               (dissoc (keyword team-1))
                                               (assoc (keyword %) 1))))
             :show-search        true
             :option-filter-prop "children"
             :style              {:min-width "128px"}}
            (select/ant-options {:options  (remove #(= (:id %) (utils/safe-name team-2)) @free-teams)
                                 :label-fn team-name-fn})]]
          [form/form-item
           {:label "Team 2"}
           [select/select
            {:value              (utils/safe-name team-2)
             :on-change          #(swap! new-round-room update :teams
                                         (fn [teams]
                                           (-> teams
                                               (dissoc (keyword team-2))
                                               (assoc (keyword %) 2))))
             :show-search        true
             :option-filter-prop "children"
             :style              {:min-width "128px"}}
            (select/ant-options {:options  (remove #(= (:id %) (utils/safe-name team-1)) @free-teams)
                                 :label-fn team-name-fn})]]
          [form/form-item
           {:label "Judges"}
           [select/select
            {:value              (:judges @new-round-room)
             :mode               "multiple"
             :on-change          #(swap! new-round-room assoc :judges (js->clj %))
             :show-search        true
             :option-filter-prop "children"
             :style              {:min-width "128px"}}
            (select/ant-options {:options  @free-judges
                                 :label-fn :name})]]
          [button/button
           {:type     "primary"
            :on-click (fn []
                        ;; TODO: validate
                        (re-frame/dispatch [:pairings/create-round-room (assoc @new-round-room
                                                                          :tournament-id tid
                                                                          :tid tid
                                                                          :round-id round-id)])
                        (reset-fn))}
           "Create"]
          [button/button
           {:on-click reset-fn}
           "Clear"]]]))))

(defn pairings-page
  []
  (let [tid         (re-frame/subscribe [:common/active-tid])
        round-id    (re-frame/subscribe [:tournament/active-round-id])
        round-rooms (re-frame/subscribe [:round/round-rooms])]
    (fn []
      ;; link to ballots
      [:div
       {:style {:overflow-y "scroll"}}
       [space/space
        {:direction "vertical"
         :style     {:width "100%"}}
        [space/space
         [button/button
          {:href   (str "/auth/api/tournaments/" @tid "/reports/rounds/" @round-id)
           :target "_blank"}
          "Export Pairings"]
         [button/button
          {:href (kee-frame/path-for [:tournament.ballots {:tid      @tid
                                                           :round-id @round-id}])}
          "Enter Round Ballots"]]
        [table/table
         {:bordered   true
          :dataSource @round-rooms
          :footer     #(reagent/as-element [new-round-room-form @tid @round-id])
          :columns    [{:title     "Room"
                        :dataIndex "room"
                        :render    (fn [value record]
                                     (reagent/as-element
                                       [room-selection (js->clj record :keywordize-keys true)]))}
                       {:title     "Team 1"
                        :dataIndex "teams"
                        :render    (fn [value record]
                                     (reagent/as-element
                                       [team-selection (js->clj record :keywordize-keys true) 1]))}
                       {:title     "Team 2"
                        :dataIndex "teams"
                        :render    (fn [value record]
                                     (reagent/as-element
                                       [team-selection (js->clj record :keywordize-keys true) 2]))}
                       {:title     "Judges"
                        :dataIndex "judges"
                        :render    (fn [value record]
                                     (reagent/as-element
                                       [judge-selection (js->clj record :keywordize-keys true)]))}
                       {:title     "Action"
                        :dataIndex "id"
                        :render    (fn [value record]
                                     (reagent/as-element
                                       [button/button
                                        {:href   (str "/auth/api/tournaments/" @tid "/rounds/" @round-id "/rooms/" value "/mail-judges")
                                         :target "_blank"}
                                        "Send email to judges"]))}]}]]])))

