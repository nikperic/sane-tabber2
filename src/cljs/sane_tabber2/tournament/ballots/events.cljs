(ns sane-tabber2.tournament.ballots.events
  (:require
    [kee-frame.core :as kee-frame]
    [re-frame.core :as re-frame]
    [reframe-utils.core :as rf-utils]
    [sane-tabber2.common :as common]))

(rf-utils/multi-generation
  rf-utils/reg-set-event
  :ballots.modals/ballot-modal-visible?)

(re-frame/reg-event-fx
  :ballots/init-page
  (fn [{:keys [db]} _]
    (let [tid      (:common/active-tid db)
          round-id (:tournament/active-round-id db)]
      {:db         (assoc db :ballots.modals/ballot-modal-visible? false)
       :dispatch-n [[:tournament/get-judges tid]
                    [:tournament/get-rooms tid]
                    [:tournament/get-schools tid]
                    [:tournament/get-teams tid]
                    [:tournament/get-rounds tid]
                    [:round/get-round-rooms tid round-id]]})))

(kee-frame/reg-controller
  :ballots/ballots-controller
  {:params (fn [route-data]
             (when (-> route-data
                       (get-in [:data :name])
                       (= :tournament.ballots))
               true))
   :start  [:ballots/init-page]})

(re-frame/reg-event-db
  :ballots/set-active-ballot
  (fn [db [_ round-room-id]]
    (let [round-rooms (:round/round-rooms db)]
      (assoc db :ballots/active-ballot (common/get-first-by-id round-rooms round-room-id)
                :ballots.modals/ballot-modal-visible? (some? round-room-id)))))