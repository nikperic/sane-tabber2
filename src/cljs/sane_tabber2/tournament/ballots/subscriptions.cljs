(ns sane-tabber2.tournament.ballots.subscriptions
  (:require
    [re-frame.core :as re-frame]
    [reframe-utils.core :as rf-utils]))

(rf-utils/multi-generation
  rf-utils/reg-basic-sub
  :ballots/active-ballot
  :ballots.modals/ballot-modal-visible?)