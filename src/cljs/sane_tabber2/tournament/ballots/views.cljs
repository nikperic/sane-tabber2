(ns sane-tabber2.tournament.ballots.views
  (:require
    [clojure.set :as set]
    [clojure.string :as string]
    [reagent.core :as reagent]
    [re-frame.core :as re-frame]
    [sane-tabber2.common :as common]
    [sane-tabber2.utils :as utils]
    [syn-antd.button :as button]
    [syn-antd.col :as col]
    [syn-antd.input :as input]
    [syn-antd.modal :as modal]
    [syn-antd.row :as row]
    [syn-antd.table :as table]
    [syn-antd.tag :as tag]
    [syn-antd.tooltip :as tooltip]))

(defn valid-number-key-press?
  [e]
  (let [cc (.-charCode e)]
    (and (>= cc 48)
         (<= cc 57))))

(defn team-label
  [round-teams n teams schools]
  (let [{:keys [team-code school-id]} (common/get-first-by-id teams (-> round-teams
                                                                        (js->clj)
                                                                        (set/map-invert)
                                                                        (get n)
                                                                        (utils/safe-name)))
        school (common/get-first-by-id schools school-id)]
    (str (:code school) " " team-code)))

(defn valid-scores?
  [scores]
  (let [team-vals    (vals scores)
        team-totals  (map #(->> %
                                (vals)
                                (filter identity)
                                (apply +))
                          team-vals)
        speaker-vals (mapcat vals team-vals)]
    (and
      (every? #(and (> % 50)
                    (< % 100)) speaker-vals)
      (apply not= team-totals))))

(def score-tooltip "Remember your scores must be greater than 50 and less than 100.")

(defn ballot-entry
  [round-room submit-fn]
  (let [all-rooms   (re-frame/subscribe [:tournament/rooms])
        all-judges  (re-frame/subscribe [:tournament/judges])
        all-teams   (re-frame/subscribe [:tournament/teams])
        all-schools (re-frame/subscribe [:tournament/schools])
        scores      (reagent/atom {})]
    (fn [{:keys [teams judges room]} submit-fn]
      [:div
       [:p [:strong "Room: "] (:name (common/get-first-by-id @all-rooms room))]
       [:p [:strong "Judges: "] (string/join ", " (map (fn [id]
                                                         (:name (common/get-first-by-id @all-judges id)))
                                                       judges))]
       [:h3 "Scores"]

       (doall
         (for [[team-id team-n] (sort-by val teams)]
           ^{:key (str team-n team-id)}
           (let [team-id (name team-id)
                 team    (common/get-first-by-id @all-teams team-id)
                 school  (common/get-first-by-id @all-schools (:school-id team))]
             [row/row
              {:gutter 16}
              [col/col
               {:span 24}
               [:p>strong (:code school) " " (:team-code team)]]

              (doall
                (for [{:keys [name speaker-n]} [{:name      (:speaker1 team)
                                                 :speaker-n :speaker1}
                                                {:name      (:speaker2 team)
                                                 :speaker-n :speaker2}]]
                  ^{:key (str team-id speaker-n)}
                  [tooltip/tooltip
                   {:title score-tooltip}
                   [col/col
                    {:span 12}
                    [:p name]
                    [(if (and (= 1 team-n)
                              (= :speaker1 speaker-n))
                       utils/with-focus
                       :<>)
                     [input/input
                      {:value          (get-in @scores [team-id speaker-n])
                       :on-change      (fn [e]
                                         (let [v (utils/target-value e)]
                                           (swap! scores assoc-in [team-id speaker-n] (when (not-empty v) (js/parseInt v)))))
                       :on-press-enter (fn []
                                         (when-not (or (not (valid-scores? @scores))
                                                       ;; TODO: hacky hard coded shit
                                                       (not= 4 (count (mapcat vals (vals @scores)))))
                                           (submit-fn scores)))
                       :on-key-press   valid-number-key-press?}]]]]))

              [col/col
               {:span 24}
               [:p "Total Score"]
               [input/input
                {:disabled true
                 :value    (->> (get @scores team-id)
                                (vals)
                                (filter number?)
                                (apply +))}]]])))

       (let [team-scores     (common/team-scores @scores)
             tie?            (apply = (vals team-scores))
             winning-team-id (->> team-scores
                                  (sort-by val)
                                  (last)
                                  (first))]

         (if tie?
           [:h4 "You cannot have a tie, please change your scores."]
           [:h4 "Winner: " (case (get teams (keyword winning-team-id))
                             1 "Appellant"
                             2 "Defendant"
                             "")]))

       [:div
        [button/button
         {:type     "primary"
          :disabled (or (not (valid-scores? @scores))
                        ;; TODO: hacky hard coded shit
                        (not= 4 (count (mapcat vals (vals @scores)))))
          :on-click #(submit-fn scores)}
         "Submit ballot"]]])))

(defn ballot-modal
  []
  (let [modal-visible? (re-frame/subscribe [:ballots.modals/ballot-modal-visible?])
        active-ballot  (re-frame/subscribe [:ballots/active-ballot])
        close-modal!   #(re-frame/dispatch [:ballots/set-active-ballot nil])]
    (fn []
      (when @modal-visible?
        [modal/modal
         {:visible         @modal-visible?
          :title           "Enter Ballot"
          :on-cancel       close-modal!
          :ok-button-props {:style {:display "none"}}}
         [ballot-entry @active-ballot
          (fn [scores]
            (when (valid-scores? @scores)
              (re-frame/dispatch [:pairings/update-round-room (assoc @active-ballot :ballot @scores)])
              (reset! scores {})
              (close-modal!)))]]))))

;; TODO: maybe room search? Not important if virtual tho..
(defn ballots-page
  []
  (let [tid         (re-frame/subscribe [:common/active-tid])
        round-id    (re-frame/subscribe [:tournament/active-round-id])
        round-rooms (re-frame/subscribe [:round/round-rooms])
        rounds      (re-frame/subscribe [:tournament/rounds])
        rooms       (re-frame/subscribe [:tournament/rooms])
        judges      (re-frame/subscribe [:tournament/judges])
        teams       (re-frame/subscribe [:tournament/teams])
        schools     (re-frame/subscribe [:tournament/schools])]
    (fn []
      [:<>
       [ballot-modal]
       ;; TODO: sort unfinished first
       [:h2 "Round " (:round-number (common/get-first-by-id @rounds @round-id))]
       [table/table
        {:dataSource @round-rooms
         :columns    [{:title     "Room"
                       :dataIndex "room"
                       :render    (fn [value record]
                                    (:name (common/get-first-by-id @rooms value)))}
                      {:title     "Team 1"
                       :dataIndex "teams"
                       :render    (fn [value record]
                                    (team-label value 1 @teams @schools))}
                      {:title     "Team 2"
                       :dataIndex "teams"
                       :render    (fn [value record]
                                    (team-label value 2 @teams @schools))}
                      {:title     "Judges"
                       :dataIndex "judges"
                       :render    (fn [value record]
                                    (string/join ", " (map (fn [id]
                                                             (:name (common/get-first-by-id @judges id)))
                                                           (js->clj value))))}
                      {:title            "Status"
                       :dataIndex        "ballot"
                       :sorter           (fn [a b]
                                           (- (if (some? (aget a "ballot")) 1 0)
                                              (if (some? (aget b "ballot")) 1 0)))
                       :defaultSortOrder "ascend"
                       :render           (fn [value record]
                                           (reagent/as-element
                                             (if (some? value)
                                               [tag/tag
                                                {:color "green"}
                                                "Ballot Entered"]
                                               [tag/tag
                                                {:color "red"}
                                                "No Ballot"])))}
                      {:title     "Actions"
                       :dataIndex "id"
                       :render    (fn [value record]
                                    (let [ballot (aget record "ballot")]
                                      (prn (aget record "ballot"))
                                      (reagent/as-element
                                        (if (some? ballot)
                                          [button/button
                                           {:type     "danger"
                                            :size     "small"
                                            :on-click #(re-frame/dispatch [:pairings/update-round-room
                                                                           (dissoc (js->clj record :keywordize-keys true) :ballot)])}
                                           "Clear Ballot"]
                                          [:<>
                                           [button/button
                                            {:type     "primary"
                                             :size     "small"
                                             :on-click #(re-frame/dispatch [:ballots/set-active-ballot value])}
                                            "Add Ballot"]
                                           [button/button
                                            {:size   "small"
                                             :href   (str "/public/tournament/" @tid "/ballot/" value)
                                             :target "_blank"}
                                            "Public Link"]]))))}]}]])))