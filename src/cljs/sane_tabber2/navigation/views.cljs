(ns sane-tabber2.navigation.views
  (:require
    [kee-frame.core :as kee-frame]
    [reagent.core :as reagent]
    [re-frame.core :as re-frame]
    [syn-antd.layout :as layout]
    [syn-antd.menu :as menu]
    [syn-antd.page-header :as page-header]
    [syn-antd.icons.home-outlined :as icons.home]
    [syn-antd.icons.plus-circle-outlined :as icons.plus]
    [syn-antd.icons.unordered-list-outlined :as icons.list]
    [syn-antd.icons.table-outlined :as icons.table]
    [syn-antd.icons.user-outlined :as icons.user]
    [syn-antd.icons.logout-outlined :as icons.logout]
    [syn-antd.icons.dashboard-outlined :as icons.dashboard]))

(def menu-items
  [{:id    :tournaments
    :label "Tournaments"
    :icon  (reagent/as-element [icons.home/home-outlined])}
   {:id    :tournaments.create
    :label "New Tournament"
    :icon  (reagent/as-element [icons.plus/plus-circle-outlined])}])

(def tournament-menu-items
  [{:id    :tournaments
    :label "Home"
    :icon  (reagent/as-element [icons.home/home-outlined])}
   {:id    :tournament.rounds
    :label "Tournament"
    :icon  (reagent/as-element [icons.dashboard/dashboard-outlined])}
   {:id    :tournament.registration
    :label "Registration"
    :icon  (reagent/as-element [icons.list/unordered-list-outlined])}
   {:id    :tournament.reporting
    :label "Reporting"
    :icon  (reagent/as-element [icons.table/table-outlined])}])

(defn- generic-menu
  [menu-items & [params]]
  [menu/menu
   {:mode          "horizontal"
    :selected-keys [@(re-frame/subscribe [:nav/page])]}
   (for [{:keys [id label icon]} menu-items]
     ^{:key (name id)}
     [menu/menu-item
      {:key id}
      [:a
       {:href (kee-frame/path-for (if params [id params] [id]))}
       icon
       label]])

   ;; Right side menu
   [menu/menu-sub-menu
    {:title "User"
     :style {:float "right"}}
    [menu/menu-item
     {:key :loopback.routes/profile}
     [:a {:href (kee-frame/path-for [:profile])}
      [icons.user/user-outlined]
      "Profile"]]
    [menu/menu-item
     [:a {:on-click #(re-frame/dispatch [:nav/redirect! "/public/api/logout"])}
      [icons.logout/logout-outlined]
      "Logout"]]]])

(defn- base-menu [] [generic-menu menu-items])
(defn- tournament-menu [tid] [generic-menu tournament-menu-items {:tid tid}])

(defn navbar []
  [layout/layout-header
   {:style {:background-color "#fff"}}
   [:span
    {:style {:float "left"}}
    [page-header/page-header
     {:title (str "ST2")}]]
   (let [tid @(re-frame/subscribe [:common/active-tid])]
     (cond
       @(re-frame/subscribe [:common/public?]) [menu/menu]
       tid [tournament-menu tid]
       :else [base-menu]))])