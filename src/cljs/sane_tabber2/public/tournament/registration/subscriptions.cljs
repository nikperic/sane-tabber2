(ns sane-tabber2.public.tournament.registration.subscriptions
  (:require
    [re-frame.core :as re-frame]
    [reframe-utils.core :as rf-utils]))

(rf-utils/multi-generation
  rf-utils/reg-basic-sub)