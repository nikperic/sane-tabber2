(ns sane-tabber2.public.tournament.registration.events
  (:require
    [kee-frame.core :as kee-frame]
    [re-frame.core :as re-frame]))

(re-frame/reg-event-db
  :registration-view/get-success
  (fn [db [_ {:keys [schools teams]}]]
    (assoc db :tournament/schools schools
              :tournament/teams teams)))

(re-frame/reg-event-fx
  :registration-view/get-data
  (fn [_ [_ tid]]
    {:reframe-utils/http {:method     :get
                          :uri        (str "/public/api/tournament/" tid "/registration-data")
                          :on-success [:registration-view/get-success]}}))

(re-frame/reg-event-fx
  :registration-view/init-page
  (fn [{:keys [db]} _]
    (let [tid (:common/active-tid db)]
      {:dispatch [:registration-view/get-data tid]})))

(kee-frame/reg-controller
  :registration-view/registration-controller
  {:params (fn [route-data]
             (when (-> route-data
                       (get-in [:data :name])
                       (= :public.tournament.registration-view))
               true))
   :start  [:registration-view/init-page]})