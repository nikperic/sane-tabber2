(ns sane-tabber2.public.tournament.registration.views
  (:require
    [re-frame.core :as re-frame]
    [reagent.core :as reagent]
    [sane-tabber2.tournament.registration.views :as registration]
    [syn-antd.table :as table]
    [clojure.string :as string]
    [syn-antd.button :as button]))

(defn speaker-props
  [n]
  (let [data-index (str "speaker" n)]
    {:title     (str "Speaker " n)
     :dataIndex data-index}))

(defn teams-review
  [tid]
  (let [teams       (re-frame/subscribe [:tournament/teams-with-school-names])
        filter-text (reagent/atom "")]
    (fn [tid]
      [:div

       [table/table
        {:bordered
         true

         :pagination
         {:defaultPageSize 50}

         :size
         :small

         :dataSource
         @teams

         :title
         (fn []
           (reagent/as-element
             [registration/text-filter-box filter-text "speaker"]))

         :columns
         [{:title     "School Code"
           :dataIndex "school-code"
           :onFilter  (registration/default-filter-fn :school-code)
           :width     "100px"}
          {:title     "School"
           :dataIndex "school-name"
           :onFilter  (registration/default-filter-fn :school-name)}
          {:title     "Team Code"
           :dataIndex "team-code"
           :width     "100px"}
          ;; TODO: allow customizable # of speakers
          (merge (speaker-props 1)
                 {:filtered      (not (string/blank? @filter-text))
                  :filteredValue (when-not (string/blank? @filter-text) [@filter-text])
                  :onFilter      (fn [value record]
                                   (or ((registration/default-filter-fn :speaker1) value record)
                                       ((registration/default-filter-fn :speaker2) value record)))})
          (speaker-props 2)

          {:title     "Status"
           :dataIndex "actions"
           :sorter    registration/signed-in-sorter
           :render    (fn [_ record]
                        (reagent/as-element
                          (if (aget record "signed-in?")
                            [button/button
                             {:type "primary"
                              :size "large"}
                             "Signed In"]
                            [button/button
                             {:type "danger"
                              :size "large"}
                             "NOT SIGNED IN"])))}]}]])))

(defn public-registration-page
  []
  (let [tid (re-frame/subscribe [:common/active-tid])]
    (re-frame/dispatch [:ws/reset-ws :tournament @tid])
    (fn []
      [:div#registration-review
       [teams-review @tid]])))