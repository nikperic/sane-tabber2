(ns sane-tabber2.public.tournament.ballot.views
  (:require
    [re-frame.core :as re-frame]
    [sane-tabber2.tournament.ballots.views :as ballots]))

(defn public-ballot-page
  []
  (let [round-room (re-frame/subscribe [:public.ballot/active-round-room])]
    (fn []
      (if-some [rr @round-room]
        [ballots/ballot-entry rr (fn [scores]
                                   (prn rr)
                                    (when (ballots/valid-scores? @scores)
                                      (re-frame/dispatch [:public.ballot/submit-ballot (assoc rr :ballot @scores)])))]
        [:div
         [:p "Thank you for filling the ballot out."]]))))