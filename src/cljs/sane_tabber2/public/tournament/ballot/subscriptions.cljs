(ns sane-tabber2.public.tournament.ballot.subscriptions
  (:require
    [re-frame.core :as re-frame]
    [reframe-utils.core :as rf-utils]))

(rf-utils/multi-generation
  rf-utils/reg-basic-sub
  :public.ballot/round-room-id
  :public.ballot/active-round-room)