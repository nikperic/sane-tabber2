(ns sane-tabber2.public.tournament.ballot.events
  (:require
    [kee-frame.core :as kee-frame]
    [re-frame.core :as re-frame]))

(re-frame/reg-event-db
  :public.ballot/receive-data
  (fn [db [_ data]]
    (merge db data)))

(re-frame/reg-event-fx
  :public.ballot/get-data
  (fn [{:keys [db]} _]
    (let [tid           (:common/active-tid db)
          round-room-id (:public.ballot/round-room-id db)]
      {:reframe-utils/http {:method     :get
                            :uri        (str "/public/api/tournament/" tid "/ballot-data/" round-room-id)
                            :on-error   [:common/message {:type    :error
                                                          :message "Unable to load page properly. If this is a valid link, contact a tournament admin"}]
                            :on-success [:public.ballot/receive-data]}})))

(re-frame/reg-event-fx
  :public.ballot/init-page
  (fn [{:keys [db]} _]
    {:db       (assoc db :common/public? true)
     :dispatch [:public.ballot/get-data]}))

(kee-frame/reg-controller
  :public.ballot/ballot-controller
  {:params (fn [route-data]
             (when (-> route-data
                       (get-in [:data :name])
                       (= :public.tournament.ballot-input))
               true))
   :start  [:public.ballot/init-page]})

(re-frame/reg-event-fx
  :public.ballot/submit-ballot
  (fn [{:keys [db]} [_ round-room]]
    {:ws/send [:round/update-round-room (assoc round-room :tid (:common/active-tid db))]
     :db      (dissoc db :public.ballot/active-round-room)}))