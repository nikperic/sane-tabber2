(ns sane-tabber2.view
  (:require
    [re-frame.core :as re-frame]
    [sane-tabber2.auth.views :as auth]
    [sane-tabber2.profile.views :as profile]
    [sane-tabber2.public.tournament.ballot.views :as public.ballot]
    [sane-tabber2.public.tournament.registration.views :as public.registration]
    [sane-tabber2.tournament.ballots.views :as ballots]
    [sane-tabber2.tournament.pairings.views :as pairings]
    [sane-tabber2.tournament.registration.views :as registration]
    [sane-tabber2.tournament.reporting.views :as reporting]
    [sane-tabber2.tournament.rounds.views :as rounds]
    [sane-tabber2.tournaments.create.views :as tournaments.create]
    [sane-tabber2.tournaments.views :as tournaments]
    [sane-tabber2.navigation.views :as navigation]
    [syn-antd.back-top :as back-top]
    [syn-antd.layout :as layout]
    [syn-antd.spin :as spin]))

(defn root-component []
  (if-let [page @(re-frame/subscribe [:nav/page])]
    (if (= :auth page)
      [auth/login-page]
      [layout/layout
       [navigation/navbar]
       [layout/layout-content
        {:style {:margin    "16px auto"
                 :width     "72rem"
                 :max-width "100%"
                 :padding   "0 32px"}}
        [back-top/back-top]
        ;; TODO: is loading indicator required for this project? Probably not, nothing computationally intensive seems to happen
        [spin/spin
         {:spinning @(re-frame/subscribe [:common/loading?])
          :tip      "Loading"}
         [:div
          {:style
           {:background-color "#ffffff"
            :padding          "24px"
            :min-height       "380px"}}

          (case page
            :auth [auth/login-page]
            :profile [profile/profile-page]
            :public.tournament.ballot-input [public.ballot/public-ballot-page]
            :public.tournament.registration-view [public.registration/public-registration-page]
            :tournaments [tournaments/tournaments-page]
            :tournaments.create [tournaments.create/create-tournament-page]
            :tournament.ballots [ballots/ballots-page]
            :tournament.pairings [pairings/pairings-page]
            :tournament.registration [registration/registration-page]
            :tournament.reporting [reporting/reporting-page]
            :tournament.rounds [rounds/rounds-page]
            [:div ""])]]]
       [layout/layout-footer]])
    [spin/spin {:tip "Loading"}]))
