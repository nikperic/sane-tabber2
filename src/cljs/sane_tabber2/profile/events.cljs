(ns sane-tabber2.profile.events
  (:require
    [re-frame.core :as re-frame]))

(re-frame/reg-event-fx
  :profile/update-password
  (fn [_ [_ params]]
    {:reframe-utils/http {:method     :post
                          :uri        "/auth/api/profile/update-password"
                          :params     params
                          :on-error   [:common/message {:type    :error
                                                        :message "Unable to update password. Are you sure your current password is correct?"}]
                          :on-success [:common/message {:type    :success
                                                        :message "Updated password!"}]}}))