(ns sane-tabber2.profile.views
  (:require
    [clojure.string :as string]
    [re-frame.core :as re-frame]
    [syn-antd.button :as button]
    [syn-antd.space :as space]
    [syn-antd.input :as input]
    [syn-antd.form :as form]))

(defn profile-page
  []
  (let []
    (fn []
      [:<>
       [:h3 "Change my password"]
       [form/form
        {:name             "change-password"
         :initial-values   {:remember true}
         :on-finish        (fn [values]
                             (re-frame/dispatch [:profile/update-password (js->clj values :keywordize-keys true)]))
         :on-finish-failed (fn [values] (js/console.log "Failed:" values))}

        [form/form-item
         {:label "Current password"
          :name  "current-password"
          :rules [{:required true
                   :message  "Please input your current password!"}]}
         [input/input-password-raw]]
        [form/form-item
         {:label "New password"
          :name  "new-password"
          :rules [{:required true
                   :message  "Please input a new password!"}]}
         [input/input-password-raw]]
        [form/form-item
         {:label "Repeat new password"
          :name  "repeat-password"
          :rules [{:required true
                   :message  "You need to confirm your new password here..."}
                  (fn [current-form]
                    #js {:validator (fn [rule value]
                                  (if (and (not (string/blank? value))
                                           (= value (.getFieldValue current-form "new-password")))
                                    (.resolve js/Promise)
                                    (.reject js/Promise "The two passwords that you entered do not match!")))})]}
         [input/input-password-raw]]
        [form/form-item
         [button/button
          {:type      "primary"
           :html-type "submit"}
          "Submit"]]]])))