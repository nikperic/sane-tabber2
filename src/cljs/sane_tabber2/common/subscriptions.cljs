(ns sane-tabber2.common.subscriptions
  (:require
    [re-frame.core :refer [reg-sub]]
    [reframe-utils.core :as rf-utils]
    [sane-tabber2.common :as common]))

(rf-utils/multi-generation
  rf-utils/reg-basic-sub
  :common/error
  :common/loading?
  :common/active-tid
  :common/public?)
