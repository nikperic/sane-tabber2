(ns sane-tabber2.common.events
  (:require
    [re-frame.core :refer [reg-event-fx reg-fx]]
    [reframe-utils.core :as rf-utils]
    [reitit.frontend.controllers :as rfc]
    [reitit.frontend.easy :as rfe]
    [sane-tabber2.validation.common :as validation-utils]
    [syn-antd.message :as message]
    [syn-antd.notification :as notification]))

(rf-utils/multi-generation
  rf-utils/reg-set-event
  :common/error
  :common/loading?
  :common/active-tid
  :common/public?)


;; Utilities for notifications and messages w/ antd
(reg-fx :common/error! (fn [message] (message/error-ant-message message)))
(reg-fx :common/success! (fn [message] (message/success-ant-message message)))
(reg-fx :common/warning! (fn [message] (message/warning-ant-message message)))
(reg-fx :common/info! (fn [message] (message/info-ant-message message)))

(def message-types {:error   :common/error!
                    :success :common/success!
                    :warning :common/warning!
                    :info    :common/info!})

(reg-event-fx
  :common/message
  (fn [_ [_ {:keys [type message]}]]
    {(get message-types type) message}))

(def notification-types {:success notification/success-ant-notification
                         :info    notification/info-ant-notification
                         :warning notification/warning-ant-notification
                         :error   notification/error-ant-notification})

(reg-fx :common/notify!
        (fn [{:keys [type] :as config}]
          ((get notification-types type notification/open-ant-notification) (clj->js config))))

(reg-event-fx
  :common/notification
  (fn [_ [_ config]]
    {:common/notify! config}))

;; TODO: on 403 redirect to /admin route
(reg-event-fx
  :common/error-handler
  (fn [_ [_ {:keys [status status-text response]}]]
    {:dispatch [:common/notification
                {:type        :error
                 :message     (str "Code " status)
                 :description (or (get response :error) status-text)
                 :duration    0}]}))

(defn notify-errors [errors custom?]
  (map (fn [error]
         [:common/notification
          {:type     :error
           :message  (validation-utils/basic-error-message error custom?)
           :duration 0}])
       errors))