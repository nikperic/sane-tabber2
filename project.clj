(defproject sane-tabber2 "0.1.0-SNAPSHOT"

  :description "FIXME: write description"
  :url "https://sanetabber.com"

  :dependencies [[ch.qos.logback/logback-classic "1.2.3"]
                 [cheshire "5.10.0"]
                 [cljs-ajax "0.8.1"]
                 [clojure.java-time "0.3.2"]
                 [com.cognitect/transit-clj "1.0.324"]
                 [com.fasterxml.jackson.core/jackson-core "2.11.3"]
                 [com.fasterxml.jackson.core/jackson-databind "2.11.3"]
                 [cprop "0.1.17"]
                 [day8.re-frame/http-fx "0.2.1"]
                 [expound "0.8.6"]
                 [kee-frame "1.1.1"]
                 [luminus-transit "0.1.2"]
                 [luminus-undertow "0.1.7"]
                 [luminus/ring-ttl-session "0.3.3"]
                 [metosin/jsonista "0.2.7"]
                 [metosin/muuntaja "0.6.7"]
                 [metosin/reitit "0.5.10"]
                 [metosin/ring-http-response "0.9.1"]
                 [mount "0.1.16"]
                 [nrepl "0.8.3"]
                 [org.clojure/clojure "1.10.1"]
                 [org.clojure/core.async "1.3.610"]
                 [org.clojure/tools.cli "1.0.194"]
                 [org.clojure/tools.logging "1.1.0"]
                 [re-frame "1.1.1"]
                 [reagent "0.10.0"]
                 [ring/ring-core "1.8.2"]
                 [ring/ring-defaults "0.3.2"]
                 [selmer "1.12.31"]
                 [com.taoensso/faraday "1.11.1"]
                 [org.clojure/data.csv "1.0.0"]
                 [com.taoensso/sente "1.16.0"]
                 [buddy "2.0.0"]
                 [syn-antd "4.8.5"]
                 [reframe-utils "0.2.2"]
                 [funcool/struct "1.4.0"]
                 [clj-sendgrid "0.1.2"]]

  :min-lein-version "2.0.0"

  :source-paths ["src/clj" "src/cljs" "src/cljc"]
  :test-paths ["test/clj"]
  :resource-paths ["resources" "target/cljsbuild"]
  :target-path "target/%s/"
  :main ^:skip-aot sane-tabber2.core

  :clean-targets ^{:protect false}
  [:target-path "target/cljsbuild"]

  :plugins [[lein-shell "0.5.0"]]

  :profiles
  {:uberjar       {:omit-source    true
                   :prep-tasks     ["compile" ["shell" "./shadow" "release" "app"]]

                   :aot            :all
                   :uberjar-name   "sane-tabber2.jar"
                   :source-paths   ["env/prod/clj" "env/prod/cljs"]
                   :resource-paths ["env/prod/resources"]}

   :dev           [:project/dev :profiles/dev]
   :test          [:project/dev :project/test :profiles/test]

   :project/dev   {:jvm-opts       ["-Dconf=dev-config.edn"]
                   :dependencies   [[binaryage/devtools "1.0.2"]
                                    [cider/piggieback "0.5.1"]
                                    [day8.re-frame/re-frame-10x "0.7.0"]
                                    [pjstadig/humane-test-output "0.10.0"]
                                    [prone "2020-01-17"]
                                    [ring/ring-devel "1.8.2"]
                                    [ring/ring-mock "0.4.0"]]
                   :plugins        [[com.jakemccrary/lein-test-refresh "0.24.1"]
                                    [jonase/eastwood "0.3.5"]
                                    [clj-dynamodb-local "0.1.2"]]


                   :source-paths   ["env/dev/clj" "env/dev/cljs" "test/cljs"]
                   :resource-paths ["env/dev/resources"]
                   :repl-options   {:init-ns user
                                    :timeout 120000}
                   :injections     [(require 'pjstadig.humane-test-output)
                                    (pjstadig.humane-test-output/activate!)]}
   :project/test  {:jvm-opts       ["-Dconf=test-config.edn"]
                   :resource-paths ["env/test/resources"]

                   }
   :profiles/dev  {}
   :profiles/test {}})
