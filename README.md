# sane-tabber2

## License

MIT License 

Copyright © 2020 Nik Peric

## TODO POST 2021 Osgoode Cup

- review registration page (public)
- user registration
- user email password reset
- switch to auth0 maybe
- add users to tournament (setting page essentially)
- variable team and speaker sizes
- donation link of sorts? Help fund the cost of the thing
- host publicly
